# Special wrapper to load the declared version of the heptools toolchain.
set(LCG_VERSION latest CACHE STRING "HepTools version (aka LCG configuration)")

# Remove the reference to this file from the cache.
#unset(LCG_TOOLCHAIN_FILE CACHE)

# Find the actual toolchain file.
find_file(LCG_TOOLCHAIN_FILE
          NAMES heptools-${LCG_VERSION}.cmake
          PATHS ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/cmake
          PATH_SUFFIXES toolchain)

if(NOT LCG_TOOLCHAIN_FILE)
  message(FATAL_ERROR "Cannot find heptools-${LCG_VERSION}.cmake.")
endif()

# Reset the cache variable to have proper documentation.
set(LCG_TOOLCHAIN_FILE ${LCG_TOOLCHAIN_FILE} CACHE FILEPATH "The CMake toolchain file" FORCE)
message(STATUS "Using toolchain file                   : ${LCG_TOOLCHAIN_FILE}")

# Suffix the effective install prefix with the LCG version
if(USE_BINARIES)
  if(DEFINED ENV{RELEASE_MODE})
  elseif(DEFINED ENV{NIGHTLY_MODE} AND DEFINED ENV{weekday})
    set(CMAKE_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX}/${LCG_VERSION}/$ENV{weekday})
  else()
    set(CMAKE_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX}/${LCG_VERSION})
  endif()
endif()

include(heptools-common)
include(${LCG_TOOLCHAIN_FILE})
# Prepare the search paths according to the versions above
LCG_prepare_paths()

