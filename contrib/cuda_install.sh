#!/bin/bash
# This script replaces all files that are not in the whitelist with links to the location in cvmfs projects
set -eux

#handle files with spaces
OIFS="$IFS"
IFS=$'\n'

CUDA_ALLOWED_FILES=$1
CUDA_PROJECT_DIR=$2
CUDA_INSTALL_DIR=$3

if [ -f "${CUDA_ALLOWED_FILES}" ]; then
    echo "File ${CUDA_ALLOWED_FILES} exists"
else
    echo "File ${CUDA_ALLOWED_FILES} not found; aborting"
    exit 1
fi

# find all real files (not links or directories)
for FILE in $(find $CUDA_INSTALL_DIR -type f); do
    # get base file and strip potential .so version
    FILEBASE=$(basename "$FILE" | sed s@\\.so[.0-9]*@.so@)
    # get the directory relative to install directory
    FILE_RELATIVE_PATH=$( echo "$FILE" | sed s@${CUDA_INSTALL_DIR}/@@ )
    # if we don't find the filename in the allowed list of files (grep failes), we make a link to projects
    grep "^${FILEBASE}" $1 || ln -v -s -f "${CUDA_PROJECT_DIR}/$FILE_RELATIVE_PATH" "$FILE"
done

# link the extra/CUPTI to normal lib64 folder, needed by cuda python packages
for FILE in $(find $CUDA_INSTALL_DIR/extras/CUPTI/lib64 -name "*.so"); do
    echo "Looking at $FILE"
    # lib64 is link to targets/<ARCH>/lib
    cd $CUDA_INSTALL_DIR/lib64
    # get the directory relative to install directory
    FILE_RELATIVE_PATH=$( echo "$FILE" | sed s@${CUDA_INSTALL_DIR}/@@ )
    ln -s ../../../$FILE_RELATIVE_PATH
done

IFS=${OIFS}
