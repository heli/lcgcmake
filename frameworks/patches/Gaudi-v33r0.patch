diff --git a/CMakeLists.txt b/CMakeLists.txt
index 2c877cac4..40a64caad 100644
--- CMakeLists.txt
+++ CMakeLists.txt
@@ -37,6 +37,7 @@ endif()
 include(${CMAKE_SOURCE_DIR}/cmake/externals.cmake)
 
 # Declare project name and version
+project(Gaudi)
 gaudi_project(Gaudi v33r0)
 
 # These tests do not really fit in a subdirectory.
diff --git a/GaudiKernel/Gaudi/Accumulators.h b/GaudiKernel/Gaudi/Accumulators.h
index e25a8e5cc..804f1a28a 100644
--- GaudiKernel/Gaudi/Accumulators.h
+++ GaudiKernel/Gaudi/Accumulators.h
@@ -11,11 +11,8 @@
 
 #pragma once
 
-// Forward declarations to avoid issues with relative include order of Gaudi/Accumulators.h and Gaudi/Chrono/Counters.h
-namespace std::chrono {
-  template <typename, typename>
-  struct duration;
-}
+#include <chrono>
+
 namespace Gaudi::Accumulators {
   template <class Rep, class Period>
   auto sqrt( std::chrono::duration<Rep, Period> d );
diff --git a/GaudiKernel/src/Lib/ChronoEntity.cpp b/GaudiKernel/src/Lib/ChronoEntity.cpp
index 7c0ce4775..ad059a1e7 100644
--- GaudiKernel/src/Lib/ChronoEntity.cpp
+++ GaudiKernel/src/Lib/ChronoEntity.cpp
@@ -19,6 +19,8 @@
 #include <cstdio>
 #include <iomanip>
 #include <iostream>
+using namespace std::literals::string_literals;
+
 // ============================================================================
 // GaudiKernel
 // ============================================================================
@@ -115,15 +117,15 @@ std::string ChronoEntity::format( const double total, const double minimal, cons
   /// @todo: cache the format
   boost::format fmt( "Tot=%2$5.3g%1$s %4$43s #=%3$3lu" );
 
-  static const auto tbl = {std::make_tuple( 500, microsecond, " [us]" ), std::make_tuple( 500, millisecond, " [ms]" ),
-                           std::make_tuple( 500, second, "  [s]" ),      std::make_tuple( 500, minute, "[min]" ),
-                           std::make_tuple( 500, hour, "  [h]" ),        std::make_tuple( 10, day, "[day]" ),
-                           std::make_tuple( 5, week, "  [w]" ),          std::make_tuple( 20, month, "[mon]" ),
-                           std::make_tuple( -1, year, "  [y]" )};
+  static const auto tbl = {std::make_tuple( 500, microsecond, " [us]"s ), std::make_tuple( 500, millisecond, " [ms]"s ),
+                           std::make_tuple( 500, second, "  [s]"s ),      std::make_tuple( 500, minute, "[min]"s ),
+                           std::make_tuple( 500, hour, "  [h]"s ),        std::make_tuple( 10, day, "[day]"s ),
+                           std::make_tuple( 5, week, "  [w]"s ),          std::make_tuple( 20, month, "[mon]"s ),
+                           std::make_tuple( -1, year, "  [y]"s )};
 
   auto i = std::find_if(
       std::begin( tbl ), std::prev( std::end( tbl ) ),
-      [&]( const std::tuple<int, double, const char*>& i ) { return total < std::get<0>( i ) * std::get<1>( i ); } );
+      [&]( const std::tuple<int, double, std::string>& i ) { return total < std::get<0>( i ) * std::get<1>( i ); } );
   long double unit = std::get<1>( *i );
   fmt % std::get<2>( *i ) % (double)( total / unit ) % number;
 
@@ -132,7 +134,7 @@ std::string ChronoEntity::format( const double total, const double minimal, cons
     boost::format fmt1( "Ave/Min/Max=%2$5.3g(+-%3$5.3g)/%4$5.3g/%5$5.3g%1$s" );
     i = std::find_if(
         std::begin( tbl ), std::prev( std::end( tbl ) ),
-        [&]( const std::tuple<int, double, const char*>& i ) { return total < std::get<0>( i ) * std::get<1>( i ); } );
+        [&]( const std::tuple<int, double, const std::string>& i ) { return total < std::get<0>( i ) * std::get<1>( i ); } );
     unit = std::get<1>( *i );
     fmt1 % std::get<2>( *i ) % (double)( mean / unit ) % (double)( rms / unit ) % (double)( minimal / unit ) %
         (double)( maximal / unit );
diff --git a/cmake/BinaryTagUtils.cmake b/cmake/BinaryTagUtils.cmake
index 36afb6f74..859e1cc82 100644
--- cmake/BinaryTagUtils.cmake
+++ cmake/BinaryTagUtils.cmake
@@ -124,6 +124,11 @@ macro(parse_binary_tag)
       set(${_variable}_COMP_VERSION)
       list(GET _out 0 ${_variable}_COMP_VERSION)
       list(REMOVE_AT _out 0)
+      if(APPLE)
+        list(GET _out 0 _second)
+        set(${_variable}_COMP_VERSION ${${_variable}_COMP_VERSION}${_second})
+        list(REMOVE_AT _out 0)
+      endif()
       foreach(_n ${_out})
         set(${_variable}_COMP_VERSION "${${_variable}_COMP_VERSION}.${_n}")
       endforeach()
@@ -175,7 +180,6 @@ function(check_compiler)
       string(LENGTH "${BINARY_TAG_COMP_VERSION}." len)
       string(SUBSTRING "${CMAKE_CXX_COMPILER_VERSION}" 0 ${len} short_version)
       if(NOT "${BINARY_TAG_COMP_VERSION}." STREQUAL short_version)
-        message(WARNING "BINARY_TAG specifies compiler version ${BINARY_TAG_COMP_VERSION}. but we got ${CMAKE_CXX_COMPILER_VERSION}")
       endif()
     endif()
   endif()
diff --git a/cmake/get_host_binary_tag.py b/cmake/get_host_binary_tag.py
index 6a7bd7dd4..453f812b5 100755
--- cmake/get_host_binary_tag.py
+++ cmake/get_host_binary_tag.py
@@ -155,7 +155,7 @@ def arch():
     # Get host flags from /proc/cpuinfo
     host_flags = set()
     if sys.platform == 'darwin':
-        for l in check_output(['sysctl', '-a']).split('\n'):
+        for l in check_output(['sysctl', '-a']).decode('utf-8').split('\n'):
             if l.startswith('machdep.cpu.features') or l.startswith(
                     'machdep.cpu.extfeatures') or l.startswith(
                         'machdep.cpu.leaf7_features'):
