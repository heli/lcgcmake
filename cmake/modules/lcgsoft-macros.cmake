include(ExternalProject)
include(CMakeParseArguments)

if(POLICY CMP0057)
  cmake_policy(SET CMP0057 NEW) # Support new IN_LIST if() operator
endif()
set (dependency_split_pattern "([^-]+)-(.+)")
find_package(Git)

#---Cache the list of binary tarfiles
if(USE_BINARIES)
  if(LCG_ADDITIONAL_REPOS STREQUAL "")
    set(binRepos ${BinURL})
  elseif(LCG_INSTALL_PREFIX STREQUAL "")
    set(binRepos ${BinURL} ${LCG_ADDITIONAL_REPOS})
  else()
    set(binRepos ${LCG_ADDITIONAL_REPOS})
  endif()
  list(LENGTH binRepos binrepo_len)
  math(EXPR lastBinrepo "${binrepo_len}-1")
  foreach(i RANGE ${lastBinrepo})
    if(NOT EXISTS ${CMAKE_BINARY_DIR}/${i}-bintarlisting.txt)
      list(GET binRepos ${i} url) 
      message("Download ${url}/summary-${LCG_system}.txt")
      file(DOWNLOAD ${url}/summary-${LCG_system}.txt ${CMAKE_BINARY_DIR}/${i}-bintarlisting.txt)
    endif()
    file(STRINGS ${CMAKE_BINARY_DIR}/${i}-bintarlisting.txt binTars${i})
  endforeach()
else()
  set(lastBinrepo 0)
  set(binTars0)
endif()

#----------------------------------------------------------------------------------------------------
#---LCGPackage_Add macro  ---------------------------------------------------------------------------
#
#   This is a wrapper of the ExternalProject_Add function customized for LCG packages
#
#     o supports the fallback to a central release area to avoid building already existing projects
#     o automatically adds the log files to the install area
#     o automatically adds the sources to the install area
#     o automatically creates a binary tarball
#     o automatically strips rpath from .so files
#     o supports bundle (python) packages collecting many targets into one dir
#        - denoted with the switch "BUNDLE_PACKAGE"
#     o supports pure binary package installations
#        - denoted with the switch "BINARY_PACKAGE"
#     o For every target it sets ${name}_home, ${name}_exists and ${name}_dependencies
#     o It uses ${name}_native_version which has to be set externally
#
#----------------------------------------------------------------------------------------------------
macro(LCGPackage_Add name)

  #---If version is not defined (package not mentioned in toolchain) skip the whole macro------------
  if(DEFINED ${name}_native_version)  # If version is not defined skip
  
  #---If user recipe defined, then replace this recipe by the user one
  if(${name} IN_LIST LCG_user_recipes)
    message(STATUS "Recipe for package '${name}' is being overwritten with recipe in ${${name}_recipe_source}")
    list(REMOVE_ITEM LCG_user_recipes ${name})
    set(LCG_user_recipes ${LCG_user_recipes} PARENT_SCOPE)
    LCGPackage_Add(${${name}_recipe})
    unset(${name}_recipe)
  else()

  #---Create ${name} global target-------------------------------------------------------------------
  add_custom_target(${name} ALL)
  add_custom_target(clean-${name})
  if(LCG_SAFE_INSTALL)
    add_custom_target(cleanmore-${name})
  endif()
  add_custom_target(${name}-sources)

  #---Loop over all versions of the package----------------------------------------------------------
  foreach( version ${${name}_native_version} )

    #---Replace conditional, expand macros and parse all the arguments-------------------------------
    LCG_expand_version_patterns(${version} ARGUMENTS "${ARGN}")
    LCG_replace_conditional_args(ARGUMENTS "${ARGUMENTS}")
    CMAKE_PARSE_ARGUMENTS(ARG "NO_STRIP_RPATH" "URL;BUNDLE_PACKAGE;BINARY_PACKAGE;PACKAGE_FINDER;BUILD_WITH_INSTRUCTION_SET"
    "REVISION;DEPENDS;DEPENDS_OPT;CONFIGURE_EXAMPLES_COMMAND;BUILD_EXAMPLES_COMMAND;INSTALL_EXAMPLES_COMMAND;TEST_COMMAND" "${ARGUMENTS}")
    string(REPLACE <VOID> "" ARGUMENTS "${ARG_UNPARSED_ARGUMENTS}")

    #--Handle the system packages--------------------------------------------------------------------
    if(DEFINED LCG_system_packages)
      if(${name} IN_LIST LCG_system_packages)
        set(${name}_in_system TRUE)
        #---Check now whether the package really exists in the system--------------------------------
        if(ARG_PACKAGE_FINDER)
          string(TOUPPER ${ARG_PACKAGE_FINDER} ARG_PACKAGE_FINDER_UPPER)
          find_package(${ARG_PACKAGE_FINDER} QUIET)
          if(${ARG_PACKAGE_FINDER}_FOUND OR ${ARG_PACKAGE_FINDER_UPPER}_FOUND)
            if(${ARG_PACKAGE_FINDER}_VERSION_STRING)
              message(STATUS "Package ${name} found in system with version ${${ARG_PACKAGE_FINDER}_VERSION_STRING}")
            elseif(${ARG_PACKAGE_FINDER}_VERSION)
              message(STATUS "Package ${name} found in system with version ${${ARG_PACKAGE_FINDER}_VERSION}")
            else()
              message(STATUS "Package ${name} found in system.")
            endif()
          else()
            message(FATAL_ERROR "Package ${name} declared as system package but it is not found")
          endif()
        endif()
      endif()
    endif()

    #---Handle multi-version packages----------------------------------------------------------------
    set(targetname ${name}-${version})

    #---remember the actual sources for the package
    if(ARG_URL)
      set(${targetname}-sources ${ARG_URL})
    endif()

    #---Handle dependencies--------------------------------------------------------------------------
    if(ARG_DEPENDS_OPT)
      foreach(dep ${ARG_DEPENDS_OPT})
        if(DEFINED ${dep}_native_version)
          list(APPEND ARG_DEPENDS ${dep})
        endif()
      endforeach()
    endif()
    set(${targetname}_dependencies "")
    if(ARG_DEPENDS)
      foreach(dep ${ARG_DEPENDS})
        if(NOT TARGET ${dep} AND NOT DEFINED ${dep}_home)
          if(DEFINED ${dep}_native_version)
            message(SEND_ERROR "Package '${name}' declares a dependency to the package '${dep}' that has not been defined. "
                               "Add a call to 'LCGPackage_set_home(${dep})' to forward declare it.")
          else()
            message(SEND_ERROR "Package '${name}' depends on package '${dep}' that has no declared version in toolchain file. "
                               "Add a call to 'LCG_external_package(${dep} <version>)' in ${LCG_TOOLCHAIN_FILE}.")
          endif()
        endif()
        if(NOT ${${dep}_home} STREQUAL TakenFromSystem)
          list(APPEND ${targetname}_dependencies ${dep})
        endif()
      endforeach()
    #---Get list of dependencies as package-version
      set (${targetname}-dependencies)
      foreach(dep ${ARG_DEPENDS})
        if(NOT ${${dep}_home} STREQUAL TakenFromSystem)
          if(dep MATCHES "${dependency_split_pattern}")
            list (APPEND ${targetname}-dependencies "${CMAKE_MATCH_1}-${CMAKE_MATCH_2}")
          else()
            list(GET ${dep}_native_version -1 dep_vers)
            list(APPEND ${targetname}-dependencies "${dep}-${dep_vers}")
          endif()
        endif()
      endforeach()
    endif()

    #---Is the static URL being replaced by a GIT repository?---------------------------------------
    if(DEFINED ${name}_${version}_GIT)
      set(_sources GIT_REPOSITORY ${${name}_${version}_GIT} 
                   GIT_TAG ${version}
                   SOURCE_DIR ${targetname}/src/${name}/${version})
    elseif(DEFINED ${name}_${version}_SRC)
      set(_sources SOURCE_DIR ${${name}_${version}_SRC})
    else()
      set(_sources URL ${ARG_URL}
                   SOURCE_DIR ${targetname}/src/${name}/${version})
    endif()

    #---Get the git revision-------------------------------------------------------------------------
    set(_args ${_sources} ${ARGUMENTS})
    list(FIND _args GIT_REPOSITORY _index)
    if(NOT _index EQUAL -1)
      math(EXPR _index "${_index} + 1")
      list(GET _args ${_index} _repo)
      list(FIND _args GIT_TAG _index)
      math(EXPR _index "${_index} + 1")
      list(GET _args ${_index} _tag)
      execute_process(COMMAND ${GIT_EXECUTABLE} ls-remote ${_repo} ${_tag}
                      OUTPUT_VARIABLE _revision
                      OUTPUT_STRIP_TRAILING_WHITESPACE)
      string(SUBSTRING "${_revision}" 0 7 _revision )
      if(DEFINED ARG_REVISION)
        set(ARG_REVISION ${_revision}|${ARG_REVISION})
      else()
        set(ARG_REVISION ${_revision})
      endif()
    endif()

    #---Get the expanded list of dependencies with their versions-------------------------------------
    LCG_get_expanded_dependencies(${targetname} ${targetname}_expanded_dependencies)
    LCG_get_full_version(${name} ${version} "${ARG_REVISION}" ${name}_full_version)
    string(SHA1 longtargethash "${${name}_full_version}")
    string(SUBSTRING "${longtargethash}" 0 5 targethash)
    set(${targetname}_hash ${targethash})

    #---Instruction set handling----------------------------------------------------------------------
    set(instructionset OFF)
    set(LCG_saved_system ${LCG_system})
    if(NOT ARG_BUILD_WITH_INSTRUCTION_SET)
      set(LCG_system ${LCG_naked_system})
    elseif(LCG_INSTRUCTIONSET)
      set(instructionset ON)
    endif()
    #---Install path----------------------------------------------------------------------------------
    #   we support both with the hash and without
    set(install_path      ${${name}_directory_name}/${version}/${LCG_system})
    set(install_hash_path ${${name}_directory_name}/${version}-${${name}-${version}_hash}/${LCG_system})

    #---Loop over all the LCG installations
    foreach(prefix ${LCG_INSTALL_PREFIX})
      if(EXISTS ${prefix}/${install_hash_path})
        set(lcg_install_path ${prefix}/${install_hash_path})
         break()
      endif()
    endforeach()

    #---Check if the version file is already existing in the installation area(s)---------------------
    set(${targetname}_version_checked 0)
    set(${targetname}_version_file ${lcg_install_path}/version.txt)
    if(EXISTS ${${targetname}_version_file})
      file(STRINGS ${${targetname}_version_file} full_version)
      if(full_version STREQUAL ${${name}_full_version})
        set(${targetname}_version_checked 1)
      endif()
    endif()

    #---Check whether the package is already available in binary form
    if(USE_BINARIES)
      set(tarname ${name}-${version}_${targethash}-${LCG_system}.tgz)
      foreach(i RANGE ${lastBinrepo})
        list(FIND binTars${i} ${tarname} _index)
        if(NOT _index EQUAL -1)
          list(GET binRepos ${i} url) 
          set(tarurl ${url}/${tarname})
          set(${targetname}_binary_exists 1)
          break()
        endif()
      endforeach()
    endif()

    #---Package to be ignord from LCG_INSTALL_PREFIX
    list(FIND LCG_IGNORE ${name} lcg_ignore)
    set(${name}_lcg_exists)
    #----------------------------------------------------------------------------------------------------------------------
    #---Check if the package is already existing in the current system
    #----------------------------------------------------------------------------------------------------------------------
    if(${name}_in_system)
      set(${name}_home TakenFromSystem)
      set(${targetname}_home ${${name}_home})
      set(no_install_path ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/system)
      add_custom_target(${targetname} ALL
                        COMMAND ${CMAKE_COMMAND} -E make_directory ${no_install_path}
                        COMMENT "${targetname} package taken from system.")
      add_custom_target(clean-${targetname} COMMAND ${CMAKE_COMMAND} -E remove ${no_install_path}
                                            COMMENT "Deleting system package ${targetname}")
      if(LCG_SAFE_INSTALL)
         add_custom_target(cleanmore-${targetname} COMMENT "${targetname} nothing to be done")
      endif()
      set(${targetname}_buildinfo "# Package ${name} is taken from the current system with HOSTNAME: ${hostname}")

    #----------------------------------------------------------------------------------------------------------------------
    #---Check if the package is already existing in the installation area(s)
    #----------------------------------------------------------------------------------------------------------------------
    elseif(lcg_ignore EQUAL -1 AND ${${targetname}_version_checked})
      set(${name}_lcg_exists ON)
      set(${name}_home ${CMAKE_INSTALL_PREFIX}/${install_path})
      set(${targetname}_home ${${name}_home})
      add_custom_target(${targetname} ALL DEPENDS ${${targetname}-dependencies} ${CMAKE_INSTALL_PREFIX}/${install_path})
      add_custom_command(OUTPUT  ${CMAKE_INSTALL_PREFIX}/${install_path}
                        COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}
                        COMMAND ${CMAKE_COMMAND} -E create_symlink ${lcg_install_path} ${CMAKE_INSTALL_PREFIX}/${install_path}
                        COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/tarfiles
                        #COMMAND ${CMAKE_COMMAND} -E chdir ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}
                        COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-start.timestamp
                        COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-stop.timestamp
                        COMMENT "${targetname} package already existing in ${lcg_install_path}. Making a soft-link.")
      add_custom_target(clean-${targetname} COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_INSTALL_PREFIX}/${install_path}
                                            COMMENT "Deleting soft-link for package ${targetname}")
      if(LCG_SAFE_INSTALL)
         #### ---- this has to be replaced by actual content if it is really needed
         add_custom_target(cleanmore-${targetname} COMMENT "${targetname} nothing to be done")
      endif()

      #---Get the buildinfo from the already installed package
      if(EXISTS ${lcg_install_path}/.buildinfo_${name}.txt)
        file(STRINGS ${lcg_install_path}/.buildinfo_${name}.txt ${targetname}_buildinfo)
        # Cope with obsolete formats of .buildinfo file in release area
        string(REPLACE "SVNREVISION:" "DIRECTORY: ${${name}_directory_name}, SVNREVISION:" ${targetname}_buildinfo ${${targetname}_buildinfo})
        string(REPLACE "GITHASH: \"format:" "DIRECTORY: ${${name}_directory_name}, GITHASH: '" ${targetname}_buildinfo ${${targetname}_buildinfo} )
        string(REPLACE "\"" "'" ${targetname}_buildinfo ${${targetname}_buildinfo})
        if(NOT ${${targetname}_buildinfo} MATCHES "DIRECTORY:")
          string(REPLACE "VERSION:" "DIRECTORY: ${${name}_directory_name}, VERSION:" ${targetname}_buildinfo ${${targetname}_buildinfo} )
        endif()
      endif()
    #----------------------------------------------------------------------------------------------------------------------
    #----Check if binary tarfile exists so if already downloaded link or download and link 
    #----------------------------------------------------------------------------------------------------------------------
    elseif(lcg_ignore EQUAL -1 AND ${targetname}_binary_exists)
      set(${name}_home ${CMAKE_INSTALL_PREFIX}/${install_path})
      get_filename_component(preprefix ${CMAKE_INSTALL_PREFIX}/.. ABSOLUTE)
      file(RELATIVE_PATH relativepath ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version} ${preprefix}/${install_hash_path})
      set(${targetname}_home ${${name}_home})
      add_custom_target(${targetname} ALL DEPENDS ${${targetname}-dependencies} ${CMAKE_INSTALL_PREFIX}/${install_path})
      if(EXISTS ${preprefix}/${install_hash_path})
        add_custom_command(OUTPUT  ${CMAKE_INSTALL_PREFIX}/${install_path}
                      COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}
                      COMMAND ${CMAKE_COMMAND} -E create_symlink ${relativepath} ${CMAKE_INSTALL_PREFIX}/${install_path}
                      COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-start.timestamp
                      COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-stop.timestamp
                      COMMENT "${targetname} binary already installed. Linking to it.")
        if(EXISTS ${preprefix}/${install_hash_path}/.buildinfo_${name}.txt)
          file(STRINGS ${preprefix}/${install_hash_path}/.buildinfo_${name}.txt ${targetname}_buildinfo)
        endif()
      else()
        add_custom_command(OUTPUT  ${CMAKE_INSTALL_PREFIX}/${install_path}
                      COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}
                      COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/install_binary_tarfile.py --url=${tarurl} --prefix=${CMAKE_INSTALL_PREFIX} --lcgprefix="${LCG_INSTALL_PREFIX}"
                      COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-start.timestamp
                      COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-stop.timestamp
                      COMMAND ${LOCKFILE} -l 200 ${CMAKE_BINARY_DIR}/summary.lock
                      COMMAND ${CMAKE_COMMAND} -DNAME=${name} 
                                               -DSUMFILE=${CMAKE_BINARY_DIR}/LCG_${LCG_VERSION}_${LCG_saved_system}.txt
                                               -DINSTALLDIR=${CMAKE_INSTALL_PREFIX}/${install_path}
                                               -P ${CMAKE_SOURCE_DIR}/cmake/scripts/UpdateSummaryFile.cmake
                      COMMAND ${LOCKFILE} --unlock ${CMAKE_BINARY_DIR}/summary.lock
                      COMMENT "${targetname} binary already existing in ${tarurl}. Downloading and installing it.")
          #---Do not have the buildinfo from the binary package
          set(${targetname}_buildinfo  "# PLACEHOLDER for package ${name}#")
      endif()
      add_custom_target(clean-${targetname} COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_INSTALL_PREFIX}/${install_path}
                                            COMMENT "Deleting binaries for package ${targetname}")
      if(LCG_SAFE_INSTALL)
        #### ---- this has to be replaced by actual content if it is really needed
        add_custom_target(cleanmore-${targetname} COMMENT "${targetname} nothing to be done")
      endif()

    #----------------------------------------------------------------------------------------------------------------------
    #---We really need to build the package
    #----------------------------------------------------------------------------------------------------------------------
    else()
      #---Set home and install name-------------------------------------------------------------------
      set(${name}_home            ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/${LCG_system})
      set(${name}-${version}_home ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/${LCG_system})

      #---Get the environment needed to build--------------------------------------------------------

      LCG_get_environment(${targetname} _environment)

      #---Remove previous sym-links------------------------------------------------------------------
      if(IS_SYMLINK ${${name}_home})
        file(REMOVE ${${name}_home})
      endif()

      #---Check if a patch file exists and apply it by default---------------------------------------
      if(NOT ARGUMENTS MATCHES PATCH_COMMAND)
        if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/patches/${name}-${version}.patch)
          # old version of `patch` makes uncopyable backup files (on SLC5 and MAC)
          # http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=558485
          if(${LCG_OS}${LCG_OSVERS} STRGREATER slc5)
            set(patch_backup_opt -b)
          else()
            set(patch_backup_opt)
          endif()

          list(APPEND ARGUMENTS PATCH_COMMAND patch -l -p0 ${patch_backup_opt} -i ${CMAKE_CURRENT_SOURCE_DIR}/patches/${name}-${version}.patch)
        endif()
      endif()

      #---We cannot use the argument INSTALL_DIR becuase cmake itself will create the directory
      #   unconditionaly before make is executed. So, we replace <INSTALL_DIR> before passing the
      #   arguments to ExternalPreject_Add().
      string(REPLACE <INSTALL_DIR> ${${name}_home} ARGUMENTS "${ARGUMENTS}")
      string(REPLACE <INSTALL_DIR> ${${name}_home} ARG_TEST_COMMAND_BIS "${ARG_TEST_COMMAND}")

      #---Deal with bundled packages by creating a packagelist.txt file--------------------------------
      if(ARG_BUNDLE_PACKAGE)
        set(_packages)
        foreach(dep ${${targetname}-dependencies})
          list(APPEND _packages ${${dep}_home})
        endforeach()
        set(_file ${CMAKE_CURRENT_BINARY_DIR}/${targetname}-packagelist.txt)
        file(WRITE ${_file} "${_packages}")
        string(REPLACE <BUNDLE_LIST> "${_file}" ARGUMENTS "${ARGUMENTS}")
      endif()

      if(LCG_BUILD_ALWAYS)
        set(_build_always BUILD_ALWAYS 1)
      else()
        set(_build_always)
      endif()

      #---Add the external project -------------------------------------------------------------------
      ExternalProject_Add(
        ${targetname}
        PREFIX ${targetname}
        ${_sources}
        "${ARGUMENTS}"
        ${_build_always}
        ENVIRONMENT ${_environment}
        TEST_COMMAND ${ARG_TEST_COMMAND_BIS}
        LOG_CONFIGURE 1 LOG_BUILD 1 LOG_INSTALL 1 )

      if(ARG_DEPENDS)
        if(${targetname}-dependencies)
          add_dependencies(${targetname} ${${targetname}-dependencies})
        endif()
      endif()

      #--Prioritize the update and patch command------------------------------------------------------
      if(ARGUMENTS MATCHES UPDATE_COMMAND AND ARGUMENTS MATCHES PATCH_COMMAND)
        ExternalProject_Get_Property(${targetname} stamp_dir)
        add_custom_command(APPEND
          OUTPUT ${stamp_dir}/${targetname}-patch
          DEPENDS ${stamp_dir}/${targetname}-update)
      endif()

      #---Add a step to check if the <INSTALL_DIR> already exists and fail eventually-----------------
      if(LCG_SAFE_INSTALL)
        #--- Add the extra check step only if it is not installed somewhere else
        ExternalProject_Add_Step(${targetname} check_install_dir
                                 COMMAND test ! -d ${${name}_home}
                                 COMMENT "Checking that install directory '${${name}_home}' does not exist.\n"
                                 DEPENDERS download)
      endif()

      #---Adding extra step to copy the sources in /share/sources-------------------------------------
      if(LCG_SOURCE_INSTALL AND NOT ARG_BINARY_PACKAGE)
         if (LCG_TARBALL_INSTALL)
            ExternalProject_Add_Step(${targetname} install_sources
              COMMENT "Installing sources for '${targetname}' and create source tarball"
              COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}
              COMMAND ${LOCKFILE} -l 1200 ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/lock.txt
              COMMAND ${CMAKE_COMMAND} -DSRC=<SOURCE_DIR> -DDST=${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/share/sources -P ${CMAKE_SOURCE_DIR}/cmake/scripts/copy.cmake
              COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/../distribution/${name}
              COMMAND ${CMAKE_COMMAND} -E chdir <SOURCE_DIR>/../.. ${CMAKE_COMMAND} -E tar cfz ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/../distribution/${name}/${name}-${version}-src.tgz ${name}
              COMMAND ${LOCKFILE} --unlock ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/lock.txt
              COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-start.timestamp
              DEPENDERS configure
              DEPENDEES update patch)
         else()
            ExternalProject_Add_Step(${targetname} install_sources
              COMMENT "Installing sources for '${targetname}'"
              COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}
              COMMAND ${LOCKFILE} -l 1200 ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/lock.txt
              COMMAND ${CMAKE_COMMAND} -DSRC=<SOURCE_DIR> -DDST=${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/share/sources -P ${CMAKE_SOURCE_DIR}/cmake/scripts/copy.cmake
              COMMAND ${LOCKFILE} --unlock ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/lock.txt
              COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-start.timestamp
              DEPENDERS configure
              DEPENDEES update patch)
         endif()
      else()
       	 ExternalProject_Add_Step(${targetname} create_share
            COMMENT "Creating share for '${targetname}'"
            COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/share
            COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-start.timestamp
            DEPENDERS configure
            DEPENDEES update patch)
      endif()

      # Record the revision number of lcgcmake
      if(NOT lcgcmakerevision)
        execute_process(COMMAND ${GIT_EXECUTABLE} log --pretty=format:'%h' -n1 HEAD -1
                        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                        OUTPUT_VARIABLE lcgcmakerevision
                        OUTPUT_STRIP_TRAILING_WHITESPACE)
        set(lcgcmakerevision ${lcgcmakerevision} CACHE INTERNAL "revision of lcgcmake")
      endif()


      #---Compile information needed for the build description file (installed alongside version.txt)
      # Change version of compiler adding fp suffix if it contains "fp" in version
      if ( "${CMAKE_CXX_COMPILER}" MATCHES ".*${CMAKE_CXX_COMPILER_VERSION}.fp.*" )
           set(buildinfostring " COMPILER: ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}.fp,")
      else()
           set(buildinfostring " COMPILER: ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION},")
      endif()

      site_name(hostname)
      set(buildinfostring "${buildinfostring} HOSTNAME: ${hostname},")
      set(buildinfostring "${buildinfostring} GITHASH: ${lcgcmakerevision},")
      set(buildinfostring "${buildinfostring} HASH: ${targethash},")
      set(buildinfostring "${buildinfostring} DESTINATION: ${name},")
      set(buildinfostring "${buildinfostring} DIRECTORY: ${${name}_directory_name},")
      set(buildinfostring "${buildinfostring} NAME: ${name},")
      set(buildinfostring "${buildinfostring} VERSION: ${version},")
      set(buildinfostring "${buildinfostring} REVISION: ${ARG_REVISION},")
      set(buildinfostring "${buildinfostring} PLATFORM: ${LCG_system},")
      if(instructionset)
        set(buildinfostring "${buildinfostring} ISET: ${instructionset},")
      endif()

      get_property(url TARGET ${targetname} PROPERTY _EP_URL)
      # TODO: Perhaps file:// should be copied to download dir before extraction.
      string(REGEX REPLACE "^file://" "" url "${url}")
      set(buildinfostring "${buildinfostring} SRC: ${url},")

      set(buildinfostring "${buildinfostring} DEPENDS: ")

      foreach(dep ${ARG_DEPENDS})
        # ignore packages taken from system
        if(NOT ${${dep}_home} STREQUAL TakenFromSystem)
          # dependent package may have the form name-version
          if(dep MATCHES "${dependency_split_pattern}")
            set(buildinfostring "${buildinfostring}${dep}-${${dep}_hash},")
          else()
            list(GET ${dep}_native_version -1 vers)
            set(buildinfostring "${buildinfostring}${dep}-${vers}-${${dep}-${vers}_hash},")
          endif()
        endif()
      endforeach()
      set(${targetname}_buildinfo "${buildinfostring}")

      #---Adding extra step to copy the log files and version file--------------------------------------
      ExternalProject_Add_Step(${targetname} install_logs COMMENT "Installing log and version files for '${targetname}'"
          COMMAND ${CMAKE_COMMAND} -DINSTALL_DIR=${${name}_home}/logs -DLOGS_DIR=${CMAKE_CURRENT_BINARY_DIR}/${targetname}/src/${targetname}-stamp
                                   -P ${CMAKE_SOURCE_DIR}/cmake/scripts/InstallLogFiles.cmake
          COMMAND ${CMAKE_COMMAND} -DNAME=${name} -DINSTALL_DIR=${${name}_home} -DBUILDINFO=${${targetname}_buildinfo} -P ${CMAKE_SOURCE_DIR}/cmake/scripts/InstallBuildinfoFile.cmake
          COMMAND ${CMAKE_COMMAND} -DINSTALL_DIR=${${name}_home} -DFULL_VERSION=${${name}_full_version} -P ${CMAKE_SOURCE_DIR}/cmake/scripts/InstallVersionFile.cmake
          DEPENDEES install)

      #---Add extra steps eventually------------------------------------------------------------------
      set(current_dependee install_logs)
      if(ARG_TEST_COMMAND)
        set(testdepender DEPENDERS test)
      endif()
      if(ARG_CONFIGURE_EXAMPLES_COMMAND)
        string(REPLACE <INSTALL_DIR> ${${name}_home} ARG_CONFIGURE_EXAMPLES_COMMAND_BIS "${ARG_CONFIGURE_EXAMPLES_COMMAND}")
        ExternalProject_Add_Step(${targetname} configure_examples COMMENT "Configure examples for '${targetname}'"
          COMMAND  ${ARG_CONFIGURE_EXAMPLES_COMMAND_BIS}
          ${testdepender}
          DEPENDEES ${current_dependee})
        set(current_dependee configure_examples)
      endif()
      if(ARG_BUILD_EXAMPLES_COMMAND)
        string(REPLACE <INSTALL_DIR> ${${name}_home} ARG_BUILD_EXAMPLES_COMMAND_BIS "${ARG_BUILD_EXAMPLES_COMMAND}")
        ExternalProject_Add_Step(${targetname} build_examples COMMENT "Build examples for '${targetname}'"
          COMMAND  ${ARG_BUILD_EXAMPLES_COMMAND_BIS}
          ${testdepender}
          DEPENDEES ${current_dependee})
        set(current_dependee build_examples)
      endif()
      if(ARG_INSTALL_EXAMPLES_COMMAND)
        string(REPLACE <INSTALL_DIR> ${${name}_home} ARG_INSTALL_EXAMPLES_COMMAND_BIS "${ARG_INSTALL_EXAMPLES_COMMAND}")
        ExternalProject_Add_Step(${targetname} install_examples COMMENT "Install examples for '${targetname}'"
          COMMAND  ${ARG_INSTALL_EXAMPLES_COMMAND_BIS}
          ${testdepender}
          DEPENDEES ${current_dependee})
        set(current_dependee install_examples)
      endif()

      #---Remove the rpath from all shared objects----------------------------------------------------
      if(APPLE)
        set(fix_mac_relocation COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/fix_mac_relocation.py ${${name}_home})
       endif()
      if (STRIP_RPATH AND NOT ARG_NO_STRIP_RPATH)
        ExternalProject_Add_Step(${targetname} strip_rpath COMMENT "Removing rpath from '${targetname}'"
          COMMAND ${CMAKE_COMMAND} -DINSTALL_DIR=${${name}_home} -DCHECK_PATH=${CMAKE_SOURCE_DIR}/cmake/scripts/check_relative_rpath.sh
                                   -P ${CMAKE_SOURCE_DIR}/cmake/scripts/RemoveRPath.cmake
          ${fix_mac_relocation}
          DEPENDEES ${current_dependee})
        set(current_dependee strip_rpath)
      endif()

      #---Process and copy environment scripts --------------------------------------------------------
      # Available variables in template:
      # - <package>_version = version of installed package and its dependencies
      # - <package>_home    = home of installed package and its dependencies
      # - gcc_source        = path to setup.sh of gcc
      # - platform          = target platform
      # Set different file name template for generators and externals
      if (${${targetname}_home} MATCHES ".*MCGenerators.*")
        set(output_name "${name}env-genser.sh")
      else()
        set(output_name "${name}-env.sh")
      endif()
      if (EXISTS "${CMAKE_SOURCE_DIR}/cmake/environment/${name}.template")
        set (append_template_name "${CMAKE_SOURCE_DIR}/cmake/environment/${name}.template")
      else()
        set (append_template_name)
      endif()
      set (template_name "${CMAKE_SOURCE_DIR}/cmake/environment/common.template")
      set (_args "-Dappend_template_name='${append_template_name}'" "-DTARGET=${${name}_home}/.env")
      ExternalProject_Add_Step(${targetname} setup_environment COMMENT "Installing environment for ${targetname}"
          COMMAND ${CMAKE_COMMAND} -E copy ${template_name} ${${name}_home}/${output_name}
          COMMAND ${CMAKE_COMMAND} ${_args} -P ${CMAKE_SOURCE_DIR}/cmake/scripts/provide-environment.cmake
          DEPENDEES ${current_dependee}
      )
      set (current_dependee setup_environment)

      # Process .post-install.sh scripts
      if (POST_INSTALL)
        set (post-install_name "${CMAKE_SOURCE_DIR}/cmake/scripts/post-install.sh")
        set (gen_post_install_info_cmd "${CMAKE_SOURCE_DIR}/cmake/scripts/gen_post_install_info.py")
        set (path_map)
        foreach (dep ${${targetname}_expanded_dependencies};${targetname})
          string(REGEX MATCH "${dependency_split_pattern}" dep_zero "${dep}")
          set (dep_name    "${CMAKE_MATCH_1}")
          set (dep_version "${CMAKE_MATCH_2}")
          set (path_map "${${dep}_home}:${${dep_name}_directory_name}/${dep_version}-${${dep}_hash}/${LCG_system} ${path_map}")
        endforeach()
        ExternalProject_Add_Step(${targetname}  copy_post-install COMMENT "Prepare post-install for ${targetname}"
            COMMAND ${CMAKE_COMMAND} -E copy ${post-install_name} ${${name}_home}/.post-install.sh
            COMMAND chmod +x ${${name}_home}/.post-install.sh
            COMMAND ${gen_post_install_info_cmd} ${CMAKE_INSTALL_PREFIX} ${${targetname}_home} "${path_map}"
            COMMAND ${CMAKE_COMMAND} -E touch ${CMAKE_BINARY_DIR}/timestamps/${targetname}-stop.timestamp
            DEPENDEES ${current_dependee}
        )
        set (current_dependee copy_post-install)
      ENDIF()

      #---Adding extra step to build the binary tarball-----------------------------------------------
      if (LCG_TARBALL_INSTALL)
        get_filename_component(n_name ${${name}_directory_name} NAME)
        ExternalProject_Add_Step(${targetname} package COMMENT "Creating binary tarball for '${targetname}'"
            COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/tarfiles
            COMMAND ${CMAKE_COMMAND} -E chdir ${CMAKE_INSTALL_PREFIX}
                    ${CMAKE_TAR}  -czf ${CMAKE_BINARY_DIR}/tarfiles/${name}-${version}_${targethash}-${LCG_system}.tgz ${${name}_directory_name}/${version}
            COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/../distribution/${name}
            COMMAND ${CMAKE_COMMAND} -E chdir ${${name}_home}/../../..
                    ${CMAKE_TAR} -czf ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/../distribution/${name}/${name}-${version}-${LCG_system}.tgz ${n_name}/${version}/${LCG_system}
            DEPENDEES ${current_dependee})
        set (current_dependee package)
      endif()

      #---Adding clean targets--------------------------------------------------------------------------
      if(LCG_SAFE_INSTALL)
        add_custom_target(clean-${targetname} COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_CURRENT_BINARY_DIR}/${targetname}
                                              COMMENT "Removing directory '${CMAKE_CURRENT_BINARY_DIR}/${targetname}'")
        add_custom_target(cleanmore-${targetname} COMMAND ${CMAKE_COMMAND} -E remove_directory ${${name}_home}
                                                  COMMENT "Removing directory '${${name}_home}'")
        add_dependencies(cleanmore-${targetname} clean-${targetname})
      else()
        add_custom_target(clean-${targetname} COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_CURRENT_BINARY_DIR}/${targetname}
                                              COMMAND ${CMAKE_COMMAND} -E remove_directory ${${name}_home}
                                              COMMENT "Removing directores '${CMAKE_CURRENT_BINARY_DIR}/${targetname}' and '${${name}_home}'")
      endif()
    endif()
    #---Adding targets to install sources---------------------------------------------------------------
    if(${targetname}-sources)
      set(source_dir ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/sources)
      get_filename_component(tarname ${${targetname}-sources} NAME)
      add_custom_target(${targetname}-sources DEPENDS ${source_dir})
      add_custom_command(OUTPUT ${source_dir}
                        COMMAND ${CMAKE_COMMAND} -E make_directory ${source_dir}
                        COMMAND ${CMAKE_COMMAND} 
                          -Durl=${${targetname}-sources} 
                          -Dsource_dir=.
                          -Dquiet=1 
                          -Dnodecompress=1
                          -P ${CMAKE_SOURCE_DIR}/cmake/scripts/DownloadURL.cmake
                          COMMAND ${CMAKE_TAR} --strip-components=1 -xzf ${CMAKE_CURRENT_BINARY_DIR}/${tarname} -C ${source_dir}
                        COMMENT "Installing sources of ${targetname} in ${source_dir}")
    else()
      add_custom_target(${targetname}-sources COMMENT "NOT installing sources for ${targetname} (directly from repository?)")
    endif()

    add_dependencies(${name} ${targetname})
    add_dependencies(clean-${name} clean-${targetname})
    add_dependencies(${name}-sources ${targetname}-sources)
    if(LCG_SAFE_INSTALL)
      add_dependencies(cleanmore-${name} cleanmore-${targetname})
    endif()

    set(${name}-${version}_home ${${name}_home} PARENT_SCOPE)
    set(${targetname}_dependencies ${${targetname}_dependencies} PARENT_SCOPE)
    set(${targetname}-dependencies ${${targetname}-dependencies} PARENT_SCOPE)
    set(${targetname}_hash ${${targetname}_hash} PARENT_SCOPE)
    set(${targetname}_buildinfo ${${targetname}_buildinfo} PARENT_SCOPE)

  endforeach()

  #---Add target to build the dependent packages of a package
  if(${targetname}_dependencies)
    add_custom_target(${name}-dependencies DEPENDS ${${targetname}_dependencies})
  endif()

  #---Prepare 'group' targets------------------------------------------------------------------------
  get_filename_component(group ${CMAKE_CURRENT_SOURCE_DIR} NAME)
  if(NOT TARGET ${group})
    add_custom_target(${group} COMMENT "Grouping ${group} global target")
    add_custom_target(clean-${group} COMMENT "Clean group target ${group}")
  endif()
  add_dependencies(${group} ${name})
  add_dependencies(clean-${group} clean-${name})

  #--- export the latest ${name}_home variable to the other subdirectories---------------------------
  set(${name}_home ${${name}_home} PARENT_SCOPE)

  #--- mark that the target exists-------------------------------------------------------------------
  set(${name}_exists 1 PARENT_SCOPE)

  #--- export the dependencies to the outside files--------------------------------------------------
  set(${targetname}_dependencies ${${targetname}_dependencies} PARENT_SCOPE)
  set(${targetname}-dependencies ${${targetname}-dependencies} PARENT_SCOPE)

  #---Instruction set handling----------------------------------------------------------------------
  if(NOT ARG_BUILD_WITH_INSTRUCTION_SET)
    set(LCG_system ${LCG_saved_system})
  endif()

  #---End if the main ifs --------------------------------------------------------------------------
  endif(${name} IN_LIST LCG_user_recipes)
  endif(DEFINED ${name}_native_version)

endmacro()

#----------------------------------------------------------------------------------------------------
# LCGPackage_create_dependency_file function
#  o creates json and dot files containing the dependency tree
#  o uses
#      ${name}_dependencies
#      ${name}_exists
#      ${targetname}_hash
#      ${targetname}_native_version
#  o TODO: does not work with multi-version packages yet
#----------------------------------------------------------------------------------------------------
function(LCG_create_dependency_files)
  set(firstdep 1)

  set(dotfile ${CMAKE_BINARY_DIR}/dependencies.dot)
  set(jsonfile ${CMAKE_BINARY_DIR}/dependencies.json)
  set(sumfile ${CMAKE_BINARY_DIR}/LCG_${LCG_VERSION}_${LCG_system}.txt)
  set(contribfile ${CMAKE_BINARY_DIR}/LCG_${LCG_VERSION}_contrib_${LCG_system}.txt)
  set(generalcontribfile ${CMAKE_BINARY_DIR}/LCG_contrib_${LCG_system}.txt)
  file(WRITE ${dotfile} "digraph lcgcmake {\n")
  file(WRITE ${jsonfile} "{\n")
  file(WRITE ${sumfile} "# Concatenation of all the .buildinfo files for a given LCG version\n")
  # add release and platform info
  file(APPEND ${jsonfile} "'description': {\n")
  file(APPEND ${jsonfile} "'version': '${LCG_VERSION}',\n")
  file(APPEND ${jsonfile} "'platform': '${LCG_system}'\n")
  file(APPEND ${jsonfile} "},\n")
  #
  file(APPEND ${jsonfile} "'packages': {\n")
  foreach(name  ${LCG_externals})
    if(NOT ${name}_exists)
      message(STATUS "BEWARE: ${name} has a version (${${name}_native_version}), but no target defined.")
    else()
      # clean up things dot doesn't understand
      # eventually replace with REGEX REPLACE
      foreach(version ${${name}_native_version})
        set(targetname ${name}-${version})
        string(REPLACE "+" "p" cleaned_name ${targetname})
        string(REPLACE "-" "_" cleaned_name ${cleaned_name})
        string(REPLACE "-" "_" cleaned_name ${cleaned_name})
        string(REPLACE "." "_" cleaned_name ${cleaned_name})
        file(APPEND ${dotfile} "_${cleaned_name}_ [label=\"${name}-${${targetname}_hash}\"];\n")
        set(json_string "'${name}-${${targetname}_hash}' : {'name': '${name}', 'version': '${version}', 'hash': '${${targetname}_hash}', 'dest_directory':'${${name}_directory_name}' ,'dependencies' : [")
        set(firstdep 1)
        foreach(dep ${${targetname}_dependencies})
          if(NOT dep MATCHES "${dependency_split_pattern}")
            list(GET ${dep}_native_version -1 vers)
            set (dep "${dep}-${vers}")
          endif()
          string(REPLACE "+" "p" cleaned_dep ${dep})
          string(REPLACE "-" "_" cleaned_dep ${cleaned_dep})
          string(REPLACE "-" "_" cleaned_dep ${cleaned_dep})
          string(REPLACE "." "_" cleaned_dep ${cleaned_dep})
          IF (firstdep EQUAL 1)
              SET (sum_prefix "")
              SET (firstdep 0)
            ELSE()
              SET (sum_prefix ",")
          ENDIF()
          # dependent package may have the form name-version
          if(dep MATCHES "${dependency_split_pattern}")
            set(json_string "${json_string} '${CMAKE_MATCH_1}-${${dep}_hash}',")
          else()
            list(GET ${dep}_native_version -1 vers)
            set(json_string "${json_string} '${dep}-${${dep}-${vers}_hash}',")
       endif()
          file(APPEND ${dotfile} "_${cleaned_name}_ -> _${cleaned_dep}_;\n")
        endforeach()
        set(json_string "${json_string} ]}")
        file(APPEND ${jsonfile} ${json_string},\n)
        file(APPEND ${sumfile} ${${targetname}_buildinfo}\n)
      endforeach()
    endif()
  endforeach()
  file(APPEND ${dotfile} "}\n")
  file(APPEND ${jsonfile} "} }\n")

  message(STATUS "Wrote package dependency information to ${dotfile}, ${jsonfile} and ${sumfile}.")

  # Create LCG_${LCG_VERSION}_contrib_${PLATFORM}.txt
  execute_process( COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/getCompilerInfo.sh OUTPUT_VARIABLE contribcontent )
  # ${generalcontribfile} is introduced to not change the current RPM build script at all
  file(WRITE ${contribfile} "${contribcontent}")
  file(WRITE ${generalcontribfile} "${contribcontent}")
  message(STATUS "Wrote compiler information to ${contribfile}.") 
endfunction()

#----------------------------------------------------------------------------------------------------
# Helper function to expand dependencies from further dependencies
#----------------------------------------------------------------------------------------------------
function(LCG_append_depends name var)
  foreach(p ${${name}-dependencies})
    #if (NOT ${p} IN_LIST ${var})
    list(FIND ${var} ${p} idx)
    if(idx EQUAL -1)
      LCG_append_depends(${p} ${var})
      list(APPEND ${var} ${p})
    endif()
  endforeach()
  set(${var} ${${var}} PARENT_SCOPE)
endfunction()
#----------------------------------------------------------------------------------------------------
# Helper function to expand dependencies from further dependencies
#----------------------------------------------------------------------------------------------------
function(LCG_get_expanded_dependencies name var)
  set(_var)
  LCG_append_depends(${name} _var)
  if(_var)
    list(REMOVE_DUPLICATES _var)
    list(SORT _var)
  endif()
  set(${var} ${_var} PARENT_SCOPE)
endfunction()

#----------------------------------------------------------------------------------------------------
# Helper function to get the full version (inclusing the version of all expanded dependencies)
#----------------------------------------------------------------------------------------------------
function(LCG_get_full_version name version revision var)
  set(_full_version "${name}=${version}")
  #---if revision, add revision
  if(revision)
    set(_full_version "${_full_version}:${revision}")
  endif()
  #---if patch, add short hash of the patch file
  if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/patches/${name}-${version}.patch)
    file(SHA1 ${CMAKE_CURRENT_SOURCE_DIR}/patches/${name}-${version}.patch _hash)
    string(SUBSTRING "${_hash}" 0 5 _hash)
    set(_full_version "${_full_version}:${_hash}")
  endif()
  #---add the list of dependencies
  foreach(p ${${name}-${version}_expanded_dependencies})
    if(p MATCHES "${dependency_split_pattern}")
      if (${p}_hash)
        set(_full_version "${_full_version}/${CMAKE_MATCH_1}=${CMAKE_MATCH_2}-${${p}_hash}")
      else()
        message(FATAL_ERROR "Hash for package ${p} is not defined")
      endif()
    else()
      message(FATAL_ERROR "Package ${p} is ill defined")
    endif()
  endforeach()
  set(${var} ${_full_version} PARENT_SCOPE)
endfunction()

#----------------------------------------------------------------------------------------------------
# Helper function to get the full environment for building
#----------------------------------------------------------------------------------------------------
function(LCG_get_environment name var)
  set(_path)
  set(_cmake)
  set(_pkg)
  set(_python)
  set(_libpath)
  foreach(dep ${${name}_expanded_dependencies})
    if(_path)
      set(_path  ${_path}:${${dep}_home}/bin)
      set(_cmake ${_cmake}:${${dep}_home})
      set(_pkg   ${_pkg}:${${dep}_home}/lib64/pkgconfig:${${dep}_home}/lib/pkgconfig)
      set(_python ${_python}:${${dep}_home}/lib/python${Python_config_version_twodigit}/site-packages)
      set(_libpath ${_libpath}:${${dep}_home}/lib64:${${dep}_home}/lib)
    else()
      set(_path PATH=${${dep}_home}/bin)
      set(_cmake CMAKE_PREFIX_PATH=${${dep}_home})
      set(_pkg PKG_CONFIG_PATH=${${dep}_home}/lib64/pkgconfig:${${dep}_home}/lib/pkgconfig)
      set(_python PYTHONPATH=${${dep}_home}/lib/python${Python_config_version_twodigit}/site-packages)
      set(_libpath LD_LIBRARY_PATH=${${dep}_home}/lib64:${${dep}_home}/lib)
    endif()
  endforeach()
  if(_path)
    set(_path  ${_path}:\$ENV{PATH})
    set(_cmake ${_cmake}:\$ENV{CMAKE_PREFIX_PATH})
    set(_pkg   ${_pkg}:\$ENV{PKG_CONFIG_PATH})
    set(_python ${_python}:\$ENV{PYTHONPATH})
    set(_libpath ${_libpath}:\$ENV{LD_LIBRARY_PATH})
  endif()
  #--Add also the installation prefix in PYTHONPATH
  set(_python ${_python}:${${name}_home}/lib/python${Python_config_version_twodigit}/site-packages)

  #--Add /usr/lib[64]/pkgconfig for the packages takenb from the system
  set(_pkg ${_pkg}:/usr/${LIBDIR_DEFAULT}/pkgconfig)

  if(APPLE)
    set(_libpath)
  endif()

  set(${var} ${_path} ${_cmake} ${_pkg} ${_python} ${_libpath} PARENT_SCOPE)
endfunction()

#---------------------------------------------------------------------------------------------------
# Helper macro to define the home of a package
#---------------------------------------------------------------------------------------------------
macro( LCGPackage_set_home name)
  foreach( version ${${name}_native_version} )
    LCGPackage_set_install_path( name )
    set(${name}_home ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/${LCG_naked_system})
    set(${name}-${version}_home ${CMAKE_INSTALL_PREFIX}/${${name}_directory_name}/${version}/${LCG_naked_system})
  endforeach()
endmacro()

macro( LCGPackage_set_install_path name)
  foreach( version ${${name}_native_version} )
    set(${name}_install_path ${${name}_directory_name}/${version}/${LCG_system})
  endforeach()
endmacro()

#---------------------------------------------------------------------------------------------------
# Helper function to expand patterns that depend of the package version
#   o First standard patterns like <VOID> <VERSION> are tried
#   o Then any available variable is also tried for max of 3 iterations
#---------------------------------------------------------------------------------------------------
function(LCG_expand_version_patterns version outvar input)
  string(REPLACE <NATIVE_VERSION> ${version} result "${input}")
  string(REPLACE <VERSION> ${version} result "${result}")
  #string(REPLACE <VOID> "" result "${result}")
  set(knownvars SOURCE_DIR INSTALL_DIR VOID BUNDLE_LIST)
  foreach(iter 1 2 3)  # 3 nested replacements
    string(REGEX MATCHALL "[^$]<[^ <>(){}]+>" vars ${result})
    foreach(var ${vars})
      string(SUBSTRING ${var} 1 -1 var)
      string(REPLACE "<" "" v ${var})
      string(REPLACE ">" "" v ${v})
      list(FIND knownvars ${v} index)
      if(DEFINED ${v})
        string(REPLACE ${var} ${${v}} result "${result}")
      elseif(iter EQUAL 3 AND index EQUAL -1)
        message(FATAL_ERROR " Could not resolve variable '<${v}>' in 'LCGPackage_Add':\n ${result}")
      endif()
    endforeach()
  endforeach()
  set(${outvar} "${result}" PARENT_SCOPE)
endfunction()

#---------------------------------------------------------------------------------------------------
# Helper function to substibute conditional code in arguments
#   o Expession like IF <condition> THEN <arguments1> [ELSE <arguments2> ] ENDIF gets
#     replaced by <arguments1> or <argummens2> depending on the conditioon
#---------------------------------------------------------------------------------------------------
function(LCG_replace_conditional_args outvar input)
  while(1)
    set(pattern)
    set(flag 0)
    set (idx 0) # incapsulate counter
    foreach(a IN LISTS input)
      if(a STREQUAL IF)
        set(flag 1)
        MATH (EXPR idx "${idx} + 1")
        list(APPEND pattern "${a}-${idx}")
      elseif(a STREQUAL ENDIF)
        list(APPEND pattern "${a}-${idx}")
        MATH (EXPR idx "${idx} - 1")
        if (idx EQUAL 0)
          break()
        endif()
      elseif(flag)
        if (a STREQUAL THEN)
          list (APPEND pattern "${a}-${idx}")
        elseif(a STREQUAL "ELSE")
          list (APPEND pattern "${a}-${idx}")
        else()
          list(APPEND pattern "${a}")
        endif()
      endif()
    endforeach()
    if(pattern)
      # Mark levels of IF statements: IF-1, THEN-1, ENDIF-1 ...
      if("${pattern}" MATCHES "IF-[0-9];(.+);THEN-\\1;(.+);ELSE-\\1;(.+);ENDIF-\\1")
        set(condition "${CMAKE_MATCH_1}")
        set(casetrue  "${CMAKE_MATCH_2}")
        set(casefalse "${CMAKE_MATCH_3}")
      elseif("${pattern}"  MATCHES "IF-[0-9];(.+);THEN-\\1;(.+);ENDIF-\\1")
        set(condition "${CMAKE_MATCH_1}")
        set(casetrue  "${CMAKE_MATCH_2}")
        set(casefalse "")
      else()
        message(FATAL_ERROR "Unsupported IF...THEN...ELSE..ENDIF construct: '${pattern}'")
      endif()
      if(${condition})
        set(value "${casetrue}")
      else()
        set(value "${casefalse}")
      endif()
      # Revert IF-# hack ...
      foreach (s IF THEN ELSE ENDIF)
        string(REGEX REPLACE "${s}-[0-9]" "${s}" pattern "${pattern}")
        string(REGEX REPLACE "${s}-[0-9]" "${s}" value "${value}")
      endforeach()
      string(REPLACE "${pattern}" "${value}" input "${input}")
    else()
      set(${outvar} "${input}" PARENT_SCOPE)
      return()
    endif()
  endwhile()
endfunction()

#-----------------------------------------------------------------------
# function LCG_add_test(<name> TEST_COMMAND cmd [arg1... ]
#                           [PRE_COMMAND cmd [arg1...]]
#                           [POST_COMMAND cmd [arg1...]]
#                           [OUTPUT outfile] [ERROR errfile]
#                           [WORKING_DIRECTORY directory]
#                           [ENVIRONMENT var1=val1 var2=val2 ...
#                           [DEPENDS test1 ...]
#                           [TIMEOUT seconds]
#                           [DEBUG]
#                           [SOURCE_DIR dir] [BINARY_DIR dir]
#                           [BUILD target] [PROJECT project]
#                           [BUILD_OPTIONS options]
#                           [PASSREGEX exp] [FAILREGEX epx]
#                           [LABELS label1 label2 ...])
#
function(LCG_add_test test)
  LCG_replace_conditional_args(ARGN "${ARGN}")
  cmake_parse_arguments(ARG
    "DEBUG"
    "TIMEOUT;BUILD;OUTPUT;ERROR;SOURCE_DIR;BINARY_DIR;PROJECT;PASSREGEX;FAILREGEX;WORKING_DIRECTORY"
    "TEST_COMMAND;PRE_COMMAND;POST_COMMAND;ENVIRONMENT;DEPENDS;LABELS;BUILD_OPTIONS"
    ${ARGN})

  if(NOT CMAKE_GENERATOR MATCHES Makefiles)
    set(_cfg $<CONFIGURATION>/)
  endif()

  #- Handle TEST_COMMAND argument
  list(LENGTH ARG_TEST_COMMAND _len)
  if(_len LESS 1)
    if(NOT ARG_BUILD)
      message(FATAL_ERROR "LCG_ADD_TEST: command is mandatory (without build)")
    endif()
  else()
    list(GET ARG_TEST_COMMAND 0 _prg)
    list(REMOVE_AT ARG_TEST_COMMAND 0)
    if(NOT IS_ABSOLUTE ${_prg})
      set(_prg ${CMAKE_CURRENT_BINARY_DIR}/${_cfg}${_prg})
    elseif(EXISTS ${_prg})
    else()
      get_filename_component(_path ${_prg} PATH)
      get_filename_component(_file ${_prg} NAME)
      set(_prg ${_path}/${_cfg}${_file})
    endif()
    set(_cmd ${_prg} ${ARG_TEST_COMMAND})
    string(REPLACE ";" "#" _cmd "${_cmd}")
  endif()

  set(_command ${CMAKE_COMMAND} -DTST=${test} -DCMD:STRING=${_cmd})

  #- Handle PRE and POST commands
  if(ARG_PRE_COMMAND)
    set(_pre ${ARG_PRE_COMMAND})
    string(REPLACE ";" "#" _pre "${_pre}")
    set(_command ${_command} -DPRE:STRING=${_pre})
  endif()
  if(ARG_POST_COMMAND)
    set(_post ${ARG_POST_COMMAND})
    string(REPLACE ";" "#" _post "${_post}")
    set(_command ${_command} -DPOST:STRING=${_post})
  endif()

  #- Handle OUTPUT, ERROR, DEBUG arguments
  if(ARG_OUTPUT)
    set(_command ${_command} -DOUT=${ARG_OUTPUT})
  endif()

  if(ARG_ERROR)
    set(_command ${_command} -DERR=${ARG_ERROR})
  endif()

  if(ARG_DEBUG)
    set(_command ${_command} -DDBG=ON)
  endif()

  if(ARG_WORKING_DIRECTORY)
    set(_command ${_command} -DCWD=${ARG_WORKING_DIRECTORY})
  endif()

  if(ARG_TIMEOUT)
    set(_command ${_command} -DTIM=${ARG_TIMEOUT})
  endif()

  #- Handle ENVIRONMENT argument
  if(ARG_ENVIRONMENT)
    string(REPLACE ";" "#" _env "${ARG_ENVIRONMENT}")
    string(REPLACE "=" "@" _env "${_env}")
    set(_command ${_command} -DENV=${_env})
  endif()

  #- Locate the test driver
  set(_driver ${CMAKE_SOURCE_DIR}/cmake/scripts/TestDriver.cmake)
  if(NOT EXISTS ${_driver})
    message(FATAL_ERROR "LCG_ADD_TEST: TestDriver.cmake not found!")
  endif()
  set(_command ${_command} -DTESTLOGDIR=${TESTLOGDIR} -P ${_driver})

  #- Now we can actually add the test
  if(ARG_BUILD)
    if(ARG_SOURCE_DIR)
      if(NOT IS_ABSOLUTE ${ARG_SOURCE_DIR})
        set(ARG_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_SOURCE_DIR})
      endif()
    else()
      set(ARG_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
    endif()
    if(ARG_BINARY_DIR)
      if(NOT IS_ABSOLUTE ${ARG_BINARY_DIR})
        set(ARG_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/${ARG_BINARY_DIR})
      endif()
    else()
      set(ARG_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR})
    endif()
    if(NOT ARG_PROJECT)
       if(NOT PROJECT_NAME STREQUAL "LCGSoft")
         set(ARG_PROJECT ${PROJECT_NAME})
       else()
         set(ARG_PROJECT ${ARG_BUILD})
       endif()
    endif()
    add_test(NAME ${test} COMMAND ${CMAKE_CTEST_COMMAND}
      --output-log ${TESTLOGDIR}/${test}.log
      --build-and-test  ${ARG_SOURCE_DIR} ${ARG_BINARY_DIR}
      --build-generator ${CMAKE_GENERATOR}
      --build-makeprogram ${CMAKE_MAKE_PROGRAM}
      --build-target ${ARG_BUILD}
      --build-project ${ARG_PROJECT}
      --build-config $<CONFIGURATION>
      --build-noclean
      --build-options ${ARG_BUILD_OPTIONS}
      --test-command ${_command} )
    set_property(TEST ${test} PROPERTY ENVIRONMENT Geant4_DIR=${CMAKE_BINARY_DIR})
    if(ARG_FAILREGEX)
      set_property(TEST ${test} PROPERTY FAIL_REGULAR_EXPRESSION "error:|(${ARG_FAILREGEX})")
    else()
      set_property(TEST ${test} PROPERTY FAIL_REGULAR_EXPRESSION "error:")
    endif()
  else()
    add_test(NAME ${test} COMMAND ${_command})
    if(ARG_FAILREGEX)
      set_property(TEST ${test} PROPERTY FAIL_REGULAR_EXPRESSION ${ARG_FAILREGEX})
    endif()
  endif()

  #- Handle TIMOUT and DEPENDS arguments
  if(ARG_TIMEOUT)
    set_property(TEST ${test} PROPERTY TIMEOUT ${ARG_TIMEOUT})
  endif()

  if(ARG_DEPENDS)
    if(${targetname}-dependencies)
      set_property(TEST ${test} PROPERTY DEPENDS ${ARG_DEPENDS})
    endif()
  endif()

  if(ARG_PASSREGEX)
    set_property(TEST ${test} PROPERTY PASS_REGULAR_EXPRESSION ${ARG_PASSREGEX})
  endif()

  if(ARG_LABELS)
    set_property(TEST ${test} PROPERTY LABELS ${ARG_LABELS})
  else()
    set_property(TEST ${test} PROPERTY LABELS Nightly)
  endif()
endfunction()
