#!/usr/bin/env python3
"""
Program to install a complete set of LCG binary tarfiles.
<pere.mato@cern.ch>
Version 1.0
"""
#-------------------------------------------------------------------------------
import os, sys, tarfile, subprocess, shutil, glob, re
import argparse

if sys.version_info[0] >= 3 :
  import urllib.request as urllib2
else:
  import urllib2

from collections import namedtuple, defaultdict
from install_binary_tarfile import install_tarfile
from buildinfo2json import parse

def tarname_from_buildinfo(buildinfo):
  with open(buildinfo, 'r') as f:
    d = parse(f.read())
  return d['NAME']+'-'+d['VERSION']+'_'+d['HASH']+'-'+d['PLATFORM']+'.tgz'

backlist = ['ROOT']

# From https://www.geeksforgeeks.org/topological-sorting/
#---A recursive function used by topologicalSort  
def topologicalSortUtil(packages, v, visited, stack): 
  # Mark the current node as visited. 
  visited[v] = True
  # Check if back listed
  if packages[v].name.split('-')[0] in backlist and any(x in packages[v].name for x in ['patches','HEAD','master']):
    print('---- Backlisted [direct]', packages[v].name)
    visited[v] = None
    return
  else:
    visited[v] = True
  # Recur for all the vertices adjacent to this vertex 
  for d in packages[v].depends:
    try:
      i = [p.name for p in packages].index(d)
    except ValueError:
      i = None
    if i and visited[i] == None :
      print('---- Backlisted [indirect]', packages[v].name)
      visited[v] = None 
      return
    elif i and visited[i] == False:
      topologicalSortUtil(packages, i, visited, stack)
  # Push current vertex to stack which stores result 
  stack.append(v)

# The function to do Topological Sort. It uses recursive topologicalSortUtil() 
def topologicalSort(packages): 
  # Mark all the vertices as not visited 
  visited = [False]*len(packages) 
  stack =[] 
  # Call the recursive helper function to store Topological 
  # Sort starting from all vertices one by one 
  for i in range(len(packages)): 
    if visited[i] == False: 
      topologicalSortUtil(packages, i, visited, stack)
  return stack

#---install_repository----------------------------------------------------------
def install_repository(url, prefix, platform, lcgprefix='', maxpack=100):
  #--  ${url}/summary-${platform}.txt
  summary = os.path.join(url, 'summary-'+ platform + '.txt')
  try:
    resp = urllib2.urlopen(summary)
    tarfiles = resp.read().decode('utf-8').split('\n')
  except urllib2.HTTPError as detail:
    print('Error downloading %s : %s' % (summary, detail))
    sys.exit(1)
  except:
    print('Unexpected error:', sys.exc_info()[0])
    sys.exit(1)

  #---If clean purged installations---------------------------------------------
  buildinfos = glob.glob(os.path.join(prefix, '*', '*', platform,'.buildinfo_*.txt')) +\
               glob.glob(os.path.join(prefix, 'MCGenerators','*','*', platform,'.buildinfo_*.txt'))
  for info in buildinfos:
    if tarname_from_buildinfo(info) not in tarfiles:
      directory = os.path.dirname(info)
      print('==== Deleting ', directory)
      shutil.rmtree(directory, ignore_errors=True)
      #---Prune empty directories-----------------------------------------------
      p = os.path.dirname(directory)
      while p != prefix :
         if not os.listdir(p):
            print('==== Removing empty directory ', p)
            os.rmdir(p)
         p = os.path.dirname(p)
  npackages = 0
  ipackages = 0
  package = namedtuple('package', ['tarfile', 'directory', 'name', 'depends'])
  packages = []

  #---Loop over all tarfiles----------------------------------------------------
  for file in tarfiles:
    if (file == '' or 'summary' in file
        or file.startswith('.sys.a#.v')):  # filter eos artifacts
      continue
    #---Obtain the hash value
    try: 
      urltarfile = os.path.join(url, file)
      hash =  os.path.splitext(file)[0].split('-')[-5].split('_')[-1]
    except:
      print("Binary tarfile name '%s' ill-formed" % file)
      continue
    #---Open the remote tarfiles to get the true dirname and version------------
    try:
      resp = urllib2.urlopen(urltarfile)
      tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
      dirname, version = os.path.split(tar.next().name)
    except urllib2.HTTPError as detail:
      print('Error accessing %s : %s' % (urltarfile, detail))
      continue
    except tarfile.ReadError as detail:
      print('Error untaring %s : %s' %(urltarfile, detail))
      continue
    except:
      print('Unexpected error:', sys.exc_info()[0])
      sys.exit(1)

    targetdir = os.path.join(prefix, dirname, version + '-' + hash, platform)
    if not os.path.exists(targetdir):
      #  Get dependencies information from tarfile to be installed
      resp = urllib2.urlopen(urltarfile)
      tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
      for member in tar:
        if member.isfile() and re.search('.buildinfo_.*\.txt', member.name):
          buildinfo = tar.extractfile(member).read().decode('utf-8')
          break
      p = parse(buildinfo)
      packages.append(package(tarfile=urltarfile, \
                              directory=targetdir,\
                              name=p['NAME']+'-'+p['VERSION']+'-'+p['HASH'],\
                              depends=p['DEPENDS']))
      npackages += 1

  #---topological sort the list of packages-------------------------------------
  stack = topologicalSort(packages)

  #---loop over the sorted tarfiles---------------------------------------------
  for i in stack:
    install_tarfile(packages[i].tarfile, prefix, lcgprefix, True, False)
    ipackages += 1

    if ipackages >= maxpack : break

  print('Installed %d packages of a total of %d packages' % (ipackages, npackages))

#---Main program----------------------------------------------------------------
if __name__ == '__main__':

  #---Parse the arguments---------------------------------------------------------
  parser = argparse.ArgumentParser()
  parser.add_argument('--url', dest='url', help='URL of the binary cache repository', required=True)
  parser.add_argument('--prefix', dest='prefix', help='prefix to the installation', required=True)
  parser.add_argument('--lcgprefix', dest='lcgprefix', help='LCG prefix to the installation', default='', required=False)
  parser.add_argument('--platform', dest='platform', help='select a given platform', required=True)
  parser.add_argument('--maxpack', dest='maxpack', type=int, help='maximum number of packages to be installed', default=100, required=False)
  
  args = parser.parse_args()

  install_repository(args.url, args.prefix, args.platform, args.lcgprefix, args.maxpack)
