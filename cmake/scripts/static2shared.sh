#!/bin/bash

# convert static library to shared
# Latyshev Grigory

stlib="$1"
shlibpath="$(dirname $stlib)"
shlibpath=$(cd $shlibpath; pwd)
shlibname="$(basename $(echo $stlib | sed 's,\.a$,.so,g'))"
if [ `uname` = Darwin ]; then
  libc=`gfortran -print-file-name=libc.dylib`
  if [ $libc = "libc.dylib" ]; then
    libc="-L`xcrun --show-sdk-path`/usr/lib -lc"
  fi
  libraries="`gfortran -print-file-name=libgfortran.dylib` $libc"
  sharedopt=-dylib 
else
  sharedopt=--shared
fi

tmpdir=$(mktemp -d)
cp $stlib $tmpdir/
cd $tmpdir
stlib="$(basename $stlib)"

ar -x $stlib
ld $sharedopt -o res.so *.o $libraries
cp res.so $shlibpath/$shlibname

cd - 1>/dev/null
rm -rf $tmpdir
