set (outscript ${TARGET})

if (EXISTS ${append_template_name})
  file (READ ${append_template_name} append_template_content)
  file (WRITE ${outscript} "${append_template_content}")
endif()
