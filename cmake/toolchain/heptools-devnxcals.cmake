#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

LCG_external_package(ROOT v6-28-00-patches GIT=http://root.cern.ch/git/root.git)


#---Finding latest versions of nxcals packages---------------------
#Initialize token for eos access:
execute_process(COMMAND ${CMAKE_SOURCE_DIR}/jenkins/kinit.sh)

set(EOS_PATH_NXCALS_DEV /eos/project-n/nxcals/swan/nxcals_testbed)
set(PATTERN_NXCALS_JAVA "data-access-libs")
set(PATTERN_NXCALS_PYTHON "extraction_api_python3-")
set(PATTERN_NXCALS_PYTHON_LEGACY "extraction_api_python3_legacy-")

# a number followed by repeated (either a dot or rc (release candidate) and another number)
set(NXCALS_VERSION_MATCH "[0-9]((\\.|rc)[0-9]+)+")

execute_process(COMMAND $ENV{SHELL} -c "ls ${EOS_PATH_NXCALS_DEV} | grep ${PATTERN_NXCALS_JAVA}          | grep -Eo \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE NXCALS_JAVA_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND $ENV{SHELL} -c "ls ${EOS_PATH_NXCALS_DEV} | grep ${PATTERN_NXCALS_PYTHON}        | grep -Eo \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE NXCALS_PYTHON_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND $ENV{SHELL} -c "ls ${EOS_PATH_NXCALS_DEV} | grep ${PATTERN_NXCALS_PYTHON_LEGACY} | grep -Eo \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE NXCALS_LEGACY_PYTHON_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)

#---Finding latest versions of acc-py packages---------------------
execute_process(COMMAND $ENV{SHELL} -c "python3 -m pip download --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch -d \"$ENV{WORKSPACE}\"/build/acc-py --no-deps jpype1")
execute_process(COMMAND $ENV{SHELL} -c "python3 -m pip download --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch -d \"$ENV{WORKSPACE}\"/build/acc-py --no-deps cmmnbuild_dep_manager")
execute_process(COMMAND $ENV{SHELL} -c "python3 -m pip download --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch -d \"$ENV{WORKSPACE}\"/build/acc-py --no-deps pytimber")

execute_process(COMMAND $ENV{SHELL} -c "ls \"$ENV{WORKSPACE}\"/build/acc-py | grep JPype1 | grep -Eo    \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE JPYPE1_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND $ENV{SHELL} -c "ls \"$ENV{WORKSPACE}\"/build/acc-py | grep cmmnbuild | grep -Eo \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE CMMNBUILD_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND $ENV{SHELL} -c "ls \"$ENV{WORKSPACE}\"/build/acc-py | grep pytimber | grep -Eo  \'${NXCALS_VERSION_MATCH}\'" OUTPUT_VARIABLE PYTIMBER_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)

#---Nxcals needs Java11--------------------------------------------
LCG_external_package(java                     11.0.11_9                     )

#---Add nxcals packages--------------------------------------------
LCG_external_package(nxcals_data_access_libs  "${NXCALS_JAVA_VERSION}"      )
LCG_external_package(nxcals_extraction_api    "${NXCALS_PYTHON_VERSION}"    )
LCG_external_package(nxcals_extraction_api_legacy    "${NXCALS_LEGACY_PYTHON_VERSION}"    )

#---Add acc-py packages--------------------------------------------
LCG_external_package(jpype1                   "${JPYPE1_VERSION}"                            )
#---Pytimber and cmmnbuild must be build together, since pytimber must create some jar files in the cmnn repo
LCG_external_package(cmmnbuild_pytimber       "${CMMNBUILD_VERSION}-${PYTIMBER_VERSION}"     )

#---Hepak needs a newer version of numpy---------------------------
LCG_external_package(numpy             1.20.3                                )

#---Remove packages not needed by nxcals--------------
LCG_remove_package(Garfield++)

# pyhf needs newer numpy
LCG_remove_package(pyhf)
