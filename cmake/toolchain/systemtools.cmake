#---define here the packages to be taken from the shystem for this OS and version
if(${LCG_OS}${LCG_OSVERS} STREQUAL centos7 AND NOT ${LCG_ARCH} STREQUAL i686)

    set(LCG_system_packages bzip2 xz uuid curl motif                 jpeg glib               gmp libedit libevent)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL centos8)

    set(LCG_system_packages uuid libxml2 zlib expat bzip2 xz curl motif libaio lz4  jpeg  gmp libedit libevent autoconf automake m4 pkgconfig)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL centos9 OR ${LCG_OS}${LCG_OSVERS} STREQUAL el9)

    set(LCG_system_packages uuid libxml2 zlib expat bzip2 xz curl motif libaio lz4  jpeg  gmp libedit libevent autoconf automake m4 pkgconfig readline)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL slc6)

    set(LCG_system_packages          uuid curl motif                 jpeg                        libedit libevent)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL ubuntu1604)

    set(LCG_system_packages bzip2 xz uuid curl motif pkg_config      jpeg                        libedit libevent)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL ubuntu1804)

    set(LCG_system_packages bzip2 xz uuid curl motif pkg_config zlib jpeg glib flex bison m4 gmp libedit libevent)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL ubuntu2004)

    set(LCG_system_packages bzip2 xz uuid curl motif pkg_config zlib jpeg glib flex bison m4 gmp libedit libevent readline)

  elseif(${LCG_OS}${LCG_OSVERS} STREQUAL ubuntu2204)

    set(LCG_system_packages bzip2 xz uuid curl motif pkg_config zlib libaio lz4 jpeg glib flex bison m4 gmp libedit libevent readline)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL fc30)

    set(LCG_system_packages bzip2 xz uuid curl motif pkg_config zlib jpeg glib flex bison m4 gmp libedit libevent)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL mac1014)

    set(LCG_system_packages               curl)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL mac1015)

    set(LCG_system_packages               curl)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL mac11)

    set(LCG_system_packages  libffi)

elseif(${LCG_OS}${LCG_OSVERS} STREQUAL mac12)

    set(LCG_system_packages  libffi m4)

endif()
