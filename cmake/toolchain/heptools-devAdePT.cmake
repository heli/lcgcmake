#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Fix some versions----------------------------------------------
if((${LCG_COMP} MATCHES gcc) AND (${LCG_COMPVERS} EQUAL 8))
  LCG_external_package(cuda     11.1      full=11.1.1_455.32.00)
else()
  LCG_external_package(cuda     11.8      full=11.8.0_520.61.05)
endif()

LCG_external_package(VecGeom    1.1.20                        )
LCG_external_package(veccore    0.8.0                          )


LCG_top_packages(cuda VecGeom veccore Vc alpaka Boost lcgenv CMake ninja doxygen pytest Geant4)

#---Remove unneeded packages that create problems------------------
LCG_remove_package(tensorflow)
LCG_remove_package(torch)
LCG_remove_package(torchvision)
LCG_remove_package(torch_cluster)
LCG_remove_package(torch_geometric)
LCG_remove_package(torch_scatter)
LCG_remove_package(torch_sparse)
LCG_remove_package(pytorch_lightning)
LCG_remove_package(torchmetrics)

LCG_remove_package(pyhf)
LCG_remove_package(onnxruntime)
LCG_remove_package(tf2onnx)

#---We don't need ROOT in dev but without a defined version CMake fails
LCG_external_package(ROOT v6.22.06 CUDA=OFF)
# cudnn not part of top_packages, remove root dependency on cudnn
# cudnn depends on CUDA version, tarball might be missing
# LCG_external_package(cudnn 7.6.5.32)

#---Overwrite the version of Geant4
LCG_external_package(Geant4 11.00.ref01)

#---We neeed Geant4 built without VecGeom, so we chnage the recipe--
LCG_user_recipe(Geant4
    URL ${GenURL}/geant4.<Geant4_native_version>.tar.gz
    CMAKE_ARGS  -DCMAKE_BUILD_TYPE=<CMAKE_BUILD_TYPE>
                -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
                -DGEANT4_BUILD_CXXSTD=<CMAKE_CXX_STANDARD>
                -DGEANT4_USE_GDML=ON
                -DXERCESC_ROOT_DIR=<XercesC_home>
                -DGEANT4_USE_SYSTEM_CLHEP=ON
                -DGEANT4_USE_G3TOG4=ON
                -DGEANT4_INSTALL_DATADIR=<Geant4_datadir>
                -DGEANT4_BUILD_TLS_MODEL=global-dynamic
                -DGEANT4_USE_SYSTEM_EXPAT=ON
                -DGEANT4_INSTALL_PACKAGE_CACHE=OFF
                -DGEANT4_USE_USOLIDS=OFF
    BUILD_COMMAND $(MAKE)
    DEPENDS     XercesC expat motif clhep
)
