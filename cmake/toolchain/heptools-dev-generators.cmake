set(MCGENPATH  MCGenerators)

# HEPMC_VERSION allows to switch between HepMC2 and HepMC3 as default HepMC library (i.e. 2 or 3)
if(NOT HEPMC_VERSION)
  set(HEPMC_VERSION 3)
endif()

LCG_external_package(apfel             3.0.6          ${MCGENPATH}/apfel )

LCG_external_package(contur            2.4.1          ${MCGENPATH}/contur)

LCG_external_package(sherpa            2.2.15        ${MCGENPATH}/sherpa   hepmc=${HEPMC_VERSION}   author=2.2.15 hepevt=200000)
if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu20)  # No MPI support for ubuntu20 native compiler
  LCG_external_package(sherpa-openmpi  2.2.15.openmpi3  ${MCGENPATH}/sherpa hepmc=${HEPMC_VERSION}  author=2.2.15 hepevt=200000)
endif()

LCG_external_package(openloops         2.1.2          ${MCGENPATH}/openloops)
LCG_external_package(herwig3           7.2.3          ${MCGENPATH}/herwig++ thepeg=2.2.3 hepmc=${HEPMC_VERSION})

LCG_external_package(yoda              1.9.7          ${MCGENPATH}/yoda  )
LCG_external_package(rivet             3.1.7          ${MCGENPATH}/rivet hepmc=${HEPMC_VERSION} author=3.1.7)

LCG_external_package(heputils          1.4.0          ${MCGENPATH}/heputils )

LCG_external_package(mcutils           1.4.0          ${MCGENPATH}/mcutils)

LCG_external_package(collier           1.2.5          ${MCGENPATH}/collier)
LCG_external_package(syscalc           1.1.7          ${MCGENPATH}/syscalc)

LCG_external_package(madgraph5amc      3.4.2.atlas1      ${MCGENPATH}/madgraph5amc author=3.4.2)

LCG_external_package(lhapdf            6.5.3          ${MCGENPATH}/lhapdf       )

if(NOT(${LCG_COMP} STREQUAL clang) AND NOT(${LCG_OS}${LCG_OSVERS} MATCHES ubuntu))
LCG_external_package(powheg-box-v2     r3744.lhcb.rdynamic    ${MCGENPATH}/powheg-box-v2 )
LCG_external_package(agile             1.5.0          ${MCGENPATH}/agile        )
endif()

LCG_external_package(feynhiggs         2.10.2         ${MCGENPATH}/feynhiggs	)
LCG_external_package(chaplin           1.2            ${MCGENPATH}/chaplin      )

LCG_external_package(pythia8           309            ${MCGENPATH}/pythia8  author=309)

LCG_external_package(looptools         2.15           ${MCGENPATH}/looptools)

LCG_external_package(vbfnlo            3.0.0beta5     ${MCGENPATH}/vbfnlo hepmc=${HEPMC_VERSION})
LCG_external_package(FORM              4.1            ${MCGENPATH}/FORM)

LCG_external_package(njet              2.0.0          ${MCGENPATH}/njet)
LCG_external_package(qgraf             3.1.4          ${MCGENPATH}/qgraf)
LCG_external_package(gosam_contrib     2.0            ${MCGENPATH}/gosam_contrib)
LCG_external_package(gosam             2.1.1          ${MCGENPATH}/gosam)

LCG_external_package(thepeg            2.2.3          ${MCGENPATH}/thepeg hepmc=${HEPMC_VERSION})

LCG_external_package(tauola++          1.1.8.atlas1   ${MCGENPATH}/tauola++ author=1.1.8 hepmc=${HEPMC_VERSION})

LCG_external_package(pythia6           429.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=200000  )

LCG_external_package(photos++          3.64           ${MCGENPATH}/photos++ hepmc=${HEPMC_VERSION})

LCG_external_package(evtgen            2.1.1          ${MCGENPATH}/evtgen       tag=R02-01-01 hepmc=${HEPMC_VERSION})

LCG_external_package(hijing            1.383bs.2      ${MCGENPATH}/hijing       )

LCG_external_package(starlight         r313           ${MCGENPATH}/starlight   )

LCG_external_package(qd                2.3.13         ${MCGENPATH}/qd          )

LCG_external_package(photos            215.4          ${MCGENPATH}/photos       )
if( NOT ${LCG_COMP} STREQUAL clang ) # Needs CLHEP
LCG_external_package(hepmcanalysis     3.4.14         ${MCGENPATH}/hepmcanalysis  author=00-03-04-14 )
endif()
LCG_external_package(mctester          1.25.1         ${MCGENPATH}/mctester hepmc=${HEPMC_VERSION} )

LCG_external_package(herwig            6.521.2        ${MCGENPATH}/herwig       )

LCG_external_package(crmc              2.0.1p5        ${MCGENPATH}/crmc  author=2.0.1 hepmc=${HEPMC_VERSION}   )

LCG_external_package(hydjet            1.8            ${MCGENPATH}/hydjet author=1_8 )
LCG_external_package(tauola            28.121.2       ${MCGENPATH}/tauola       )

LCG_external_package(jimmy             4.31.3         ${MCGENPATH}/jimmy        )
LCG_external_package(hydjet++          2.1            ${MCGENPATH}/hydjet++ author=2_1)
LCG_external_package(alpgen            2.1.4          ${MCGENPATH}/alpgen author=214 )
LCG_external_package(pyquen            1.5.1          ${MCGENPATH}/pyquen author=1_5)
LCG_external_package(baurmc            1.0            ${MCGENPATH}/baurmc       )

LCG_external_package(professor         2.3.2          ${MCGENPATH}/professor    )

LCG_external_package(jhu               5.6.3          ${MCGENPATH}/jhu          )

#LCG_external_package(log4cpp           2.9.1          ${MCGENPATH}/log4cpp     )

LCG_external_package(rapidsim          1.4.4          ${MCGENPATH}/rapidsim     )

LCG_external_package(superchic         4.2p1          ${MCGENPATH}/superchic   author=4.2 )

if(NOT ${LCG_OS} MATCHES ubuntu|mac)
LCG_external_package(hoppet              1.2.0          ${MCGENPATH}/hoppet        )
endif()
if(NOT ${LCG_OS} MATCHES mac)
LCG_external_package(cepgen              1.1.0          ${MCGENPATH}/cepgen        )
LCG_external_package(hto4l               2.02           ${MCGENPATH}/hto4l         )
LCG_external_package(prophecy4f          3.0.2          ${MCGENPATH}/prophecy4f    )
endif()

LCG_external_package(ampt                2.26t9b_atlas  ${MCGENPATH}/ampt author=2.26t9b )

LCG_external_package(recola_SM           2.2.2          ${MCGENPATH}/recola_SM      )
LCG_external_package(recola              2.2.0          ${MCGENPATH}/recola         )
if(${LCG_COMP} STREQUAL gcc AND (${LCG_COMPVERS} VERSION_LESS 9 OR ${LCG_COMPVERS} VERSION_EQUAL 62))
LCG_external_package(hjets               1.2  ${MCGENPATH}/hjets author=1.2)
# error: ‘const class std::complex<ThePEG::Qty<std::ratio<0>, std::ratio<0>, std::ratio<0> > >’ has no member named ‘__rep’
endif()

LCG_external_package(thep8i              2.0.1         ${MCGENPATH}/thep8i  )

if(${LCG_OS} MATCHES mac)
  LCG_remove_package(sherpa-openmpi)
endif()


