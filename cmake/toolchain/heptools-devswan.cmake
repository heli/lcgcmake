#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev3)

LCG_external_package(java              11.0.14p1                                  )
if(${LCG_COMP} MATCHES gcc AND ${LCG_COMPVERS} EQUAL 8)
  # dd4hep master no longer compiles with gcc8, latest tag not compatible with root master
  LCG_external_package(DD4hep 665d9defdddba9fc4a66b613bdcae42a2eea9d77 GIT=https://github.com/AIDASoft/DD4hep.git   )

endif()

# SWAN special packages
if( ${LCG_OS}${LCG_OSVERS} MATCHES centos7 AND ${LCG_COMP} MATCHES gcc )
  LCG_external_package(c_ares            1.17.1                                   )
  LCG_external_package(condor            8.9.11                                   )
  LCG_external_package(linux_pam         1.5.1                                    )
  LCG_external_package(myschedd          1.7-2                                    )
  LCG_external_package(ngbauth_submit    0.25-2                                   )
  LCG_external_package(dask_lxplus       0.2.3                                    )
  LCG_external_package(GraalVM           22.0.0.2                                 )
endif()

# new packages for kfp
LCG_external_package(docstring_parser             0.15    )
LCG_external_package(fire                         0.4.0   )
LCG_external_package(google_api_core              2.10.1  )
LCG_external_package(google_api_python_client     1.12.11 )
LCG_external_package(googleapis_common_protos     1.56.4  )
LCG_external_package(kfp                          1.8.14  )
LCG_external_package(kfp_pipeline_spec            0.1.16  )
LCG_external_package(kfp_server_api               1.8.5   )
LCG_external_package(requests_toolbelt            0.9.1   )
LCG_external_package(strip_hints                  0.1.10  )
LCG_external_package(typer                        0.6.1   )
LCG_external_package(uritemplate                  3.0.1   )

# override versions for existing packages
LCG_external_package(cloudpickle       2.2.0                                    )
LCG_external_package(google_auth       1.35.0                                   )
LCG_external_package(protobuf          3.20.1                                   )
