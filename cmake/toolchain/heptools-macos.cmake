if(APPLE)

  LCG_external_package(libuv      1.44.2)     # Needed by horovod
  LCG_external_package(appnope    0.1.2)

  LCG_remove_package(catboost)
  LCG_remove_package(cx_oracle)
  LCG_remove_package(Davix)
  LCG_remove_package(Frontier_Client)
  LCG_remove_package(gdb)
  LCG_remove_package(GitCondDB)
  LCG_remove_package(heaptrack)
  LCG_remove_package(herwig3)
  LCG_remove_package(kokkos)
  #LCG_remove_package(mpi4py)
  LCG_remove_package(octave)
  LCG_remove_package(octavekernel)
  LCG_remove_package(openloops)               # fails building on MacOS (compiler crash)
  #LCG_remove_package(openmpi)
  LCG_remove_package(oracle)
  LCG_remove_package(pango)
  LCG_remove_package(prctl)
  LCG_remove_package(PyHEADTAIL)
  LCG_remove_package(qcachegrind)
  LCG_remove_package(R)                       # Clash between R and ROOT headers (case insensitive) 
  LCG_remove_package(rpy2)                    #   ...
  LCG_remove_package(scikitimage)
  LCG_remove_package(sherpa)
  LCG_remove_package(sherpa-openmpi)
  LCG_remove_package(tensorflow_probability)
  LCG_remove_package(unigen)                  # lonk error
  LCG_remove_package(valgrind)
  LCG_remove_package(xgboost)

  if(LCG_ARCH MATCHES arm64)
    LCG_remove_package(gosam)
    LCG_remove_package(gosam_contrib)
    LCG_remove_package(madgraph5amc)
    LCG_remove_package(madx)
    LCG_remove_package(cpymad)
    LCG_remove_package(C50)
    LCG_remove_package(llvmlite)                # developers started working on M1
    LCG_remove_package(numba)                   # ...
    LCG_remove_package(coffea)                  # ...

    LCG_remove_package(itk_core)                # installs a version of numpy, which gets relocated and produces a codesign error (Killed 9)
    LCG_remove_package(itk_io)                  # ...
    LCG_remove_package(itk_numerics)            # ...
    LCG_remove_package(itk_filtering)           # ...
    LCG_remove_package(itk_registration)        # ...
    LCG_remove_package(itk_segmentation)        # ...
    LCG_remove_package(itk)                     # ...
    LCG_remove_package(itk_meshtopolydata)      # does not exists binary
    LCG_remove_package(itkwidgets)              # ...

    LCG_remove_package(httpstan)                # no wheel for httpstan package, which is needed by pystan
    LCG_remove_package(pystan)                  # no wheel for httpstan package, which is needed by pystan
    LCG_remove_package(kubernetes_asyncio)      # ...
    LCG_remove_package(dask_kubernetes)         # ...  
    LCG_remove_package(onnxruntime)             # the wheel created is x86_64 only
    LCG_remove_package(tf2onnx)                 # ...

    LCG_remove_package(tensorflow)              # no binaries available for arm64
    LCG_remove_package(tensorflow_estimator)    # missing tensorflow
    LCG_remove_package(tensorflow_model_optimization) # missing tensorflow
    LCG_remove_package(tensorflow_io_gcs_filesystem)  # missing tensorflow
    LCG_remove_package(pyhf)                    # missing tensorflow
    LCG_remove_package(tensorboard_data_server) # missing arm64 wheel
    LCG_remove_package(tensorboard)             # missing tensorboard_data_server
    LCG_remove_package(tensorboard_plugin_wit)  # missing tensorflow
    LCG_remove_package(keras_tuner)             # missing tensorboard
    LCG_remove_package(QKeras)                  # missing tensorflow_model_optimization
  endif()
endif()
