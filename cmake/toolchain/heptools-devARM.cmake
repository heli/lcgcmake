#---There are a number of packages not supported with ARM64--------
LCG_remove_package(arrow)
LCG_remove_package(catboost)
LCG_remove_package(coffea)
LCG_remove_package(cpymad)
LCG_remove_package(llvmlite)
LCG_remove_package(numba)
LCG_remove_package(pyarrow)
LCG_remove_package(pystan)
LCG_remove_package(httpstan)
LCG_remove_package(tensorflow)
LCG_remove_package(pyhf)
LCG_remove_package(kubernetes_asyncio)
LCG_remove_package(dask_kubernetes)
LCG_remove_package(dcap)
LCG_remove_package(gfal)
LCG_remove_package(srm_ifce)

LCG_remove_package(R)

# nodejs and hidden dependents?
#LCG_remove_package(nodejs)
LCG_remove_package(panel)

# hadoop and dependents
LCG_remove_package(hadoop)
LCG_remove_package(hadoop_xrootd)
LCG_remove_package(spark)

# gomacro and dependents
LCG_remove_package(gomacro)
LCG_remove_package(gophernotes)

# has issues with pythia8
LCG_remove_package(delphes)
LCG_remove_package(cepgen)
LCG_remove_package(EDM4hep)

# python packages missing wheel (and dependents)
LCG_remove_package(dm_tree)
LCG_remove_package(tensorflow_io_gcs_filesystem)
LCG_remove_package(tensorflow_model_optimization)
LCG_remove_package(tensorflow_probability)
LCG_remove_package(QKeras)


# torch and dependents
LCG_remove_package(torch)
LCG_remove_package(torch_cluster)
LCG_remove_package(torchvision)
LCG_remove_package(torch_geometric)
LCG_remove_package(torch_scatter)
LCG_remove_package(torch_sparse)
LCG_remove_package(onnxruntime)
LCG_remove_package(pytorch_lightning)
LCG_remove_package(torchmetrics)
LCG_remove_package(tf2onnx)

LCG_remove_package(fsspec)
LCG_remove_package(dask)
LCG_remove_package(dask_jobqueue)
LCG_remove_package(distributed)


#
LCG_remove_package(kokkos)

# incredibly old auto-config (2001)
LCG_remove_package(xqilla)
LCG_remove_package(omniorb)

LCG_remove_package(libclang)
LCG_remove_package(jaxlib)
LCG_remove_package(jax)
# end python packages


# currently does not compile on aarch64
# https://github.com/JuliaPy/Conda.jl/issues/228
LCG_remove_package(jl_ijulia)


# itk and friends
LCG_remove_package(itk)
LCG_remove_package(itk_core)
LCG_remove_package(itk_filtering)
LCG_remove_package(itk_io)
LCG_remove_package(itk_meshtopolydata)
LCG_remove_package(itk_numerics)
LCG_remove_package(itk_registration)
LCG_remove_package(itk_segmentation)
LCG_remove_package(itkwidgets)

#---Overwrites of versions ----------(because ARM) ----------------
LCG_external_package(blas      0.3.17.openblas target=ARMV8)
LCG_external_package(oracle    19.10.0.0.0 )
