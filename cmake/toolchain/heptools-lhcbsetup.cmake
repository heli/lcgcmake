
LCG_user_recipe(
    DD4hep
    URL ${GenURL}/DD4hep-${DD4hep_native_version}.tar.gz
    IF DEFINED Vc_native_version THEN
        ENVIRONMENT ROOT_INCLUDE_PATH=<Vc_home>/include
    ENDIF
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=<CMAKE_BUILD_TYPE>
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DBOOST_ROOT=<Boost_home>
               -DCMAKE_CXX_STANDARD=<CMAKE_CXX_STANDARD>
               -DDD4HEP_USE_XERCESC=ON
               -DXERCESC_ROOT_DIR=<XercesC_home>
               -DROOTSYS=${ROOT_home}
               IF hepmc3_native_version THEN
                  -DDD4HEP_USE_HEPMC3=ON
               ENDIF
               IF EDM4hep_native_version THEN
                  -DDD4HEP_USE_EDM4HEP=ON
               ENDIF
               -DBUILD_DOCS=OFF
               -DDD4HEP_USE_LCIO=ON
               -DDD4HEP_USE_TBB=ON
               -DDD4HEP_LOAD_ASSIMP=OFF
	        -DD4HEP_USE_GEANT4=OFF
               -DDD4HEP_USE_GEANT4_UNITS=ON
               <Boost_extra_configuration>
    BUILD_COMMAND ${MAKE} 
    DEPENDS     ROOT XercesC Boost Python LCIO tbb
    DEPENDS_OPT assimp
      IF DD4hep_native_version STREQUAL master OR DD4hep_native_version VERSION_GREATER_EQUAL 1.13 THEN hepmc3 ENDIF
      IF DD4hep_native_version STREQUAL master OR DD4hep_native_version VERSION_GREATER_EQUAL 1.15 THEN EDM4hep ENDIF
    REVISION 3
)

# special versions of specific packages
LCG_external_package(xgboost         0.90      )


if(NOT (${LCG_COMP} STREQUAL clang))
LCG_user_recipe(powheg-box-v2
  URL http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/powheg-box-v2-<powheg-box-v2_<NATIVE_VERSION>_author>.tar.gz
  UPDATE_COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/generators/powheg-box-v2/files <SOURCE_DIR>
         COMMAND ${CMAKE_COMMAND} -E chdir <SOURCE_DIR> ./distr-cleanup.sh
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND make -f Makefile.lhcb -j1 FCOMP=${CMAKE_Fortran_COMPILER} CCOMP=${CMAKE_CXX_COMPILER} LHAPDF=<lhapdf_home> FASTJET=<fastjet_home>
  INSTALL_COMMAND make -f Makefile.lhcb install PREFIX=<INSTALL_DIR>
  IF NOT CMAKE_BUILD_TYPE MATCHES Deb THEN
          COMMAND $ENV{SHELL} -c "find <INSTALL_DIR>/lib -type f | grep -v setup | xargs -n 1 strip -S"
  ENDIF
  BUILD_IN_SOURCE 1
  DEPENDS fastjet lhapdf
)
  LCG_external_package(powheg-box-v2     r3744.lhcb2.rdynamic  ${MCGENPATH}/powheg-box-v2)
else()
  LCG_remove_package(powheg-box-v2)
endif()

# generator versions from LHCB_7
LCG_external_package(lhapdf            6.2.3                ${MCGENPATH}/lhapdf author=6.2.3 usecxxstd=1 )
LCG_external_package(photos++          3.56.lhcb1           ${MCGENPATH}/photos++ author=3.56 hepmc=2 )
LCG_external_package(pythia6           427.2.lhcb           ${MCGENPATH}/pythia6 author=6.4.27 hepevt=200000 )
LCG_external_package(tauola++          1.1.6b.lhcb          ${MCGENPATH}/tauola++ author=1.1.6b hepmc=2)
LCG_external_package(crmc              1.8.0.lhcb           ${MCGENPATH}/crmc author=1.8.0 hepmc=2)
LCG_external_package(starlight         r313                 ${MCGENPATH}/starlight )
LCG_external_package(yoda              1.9.0                ${MCGENPATH}/yoda)
LCG_external_package(rivet             3.1.4                ${MCGENPATH}/rivet yoda=1.9.0 hepmc=2)
LCG_external_package(madgraph5amc      2.9.3.atlas1         ${MCGENPATH}/madgraph5amc author=2.9.3)
LCG_external_package(thepeg            2.2.1                ${MCGENPATH}/thepeg hepmc=2)
LCG_external_package(herwig3           7.2.1                ${MCGENPATH}/herwig++  thepeg=2.2.1 madgraph=2.6.7 openloops=2.1.1 lhapdf=6.2.3 hepmc=2)
LCG_external_package(pythia8           244.lhcb4            ${MCGENPATH}/pythia8 author=244 )

FILE(DOWNLOAD ${LHCB_JSON_FILE} ${CMAKE_BINARY_DIR}/lhcb_externals.json)
FILE(READ ${CMAKE_BINARY_DIR}/lhcb_externals.json lhcb_externals_json)

execute_process(
COMMAND echo ${lhcb_externals_json}
COMMAND jq .heptools.packages[]
COMMAND xargs
OUTPUT_VARIABLE LHCB_TOP_PACKAGES
OUTPUT_STRIP_TRAILING_WHITESPACE
)

string(REPLACE "jinja2" "Jinja2" LHCB_TOP_PACKAGES ${LHCB_TOP_PACKAGES})
string(REPLACE " " ";" LHCB_TOP_PACKAGES ${LHCB_TOP_PACKAGES})
LIST(APPEND LHCB_TOP_PACKAGES DD4hep)
LIST(APPEND LHCB_TOP_PACKAGES lcgenv)
LIST(APPEND LHCB_TOP_PACKAGES GitCondDB)
LIST(APPEND LHCB_TOP_PACKAGES ipyparallel)  # needed for ipykernel, but circular dependencies

# drop powheg-box-v2 for clang from the list, because not possible for clang
if(LCG_COMP STREQUAL clang)
  LIST(REMOVE_ITEM LHCB_TOP_PACKAGES powheg-box-v2)
endif()

# special configurations needed for ARM setup
if(LCG_ARCH MATCHES "aarch64")
  LIST(REMOVE_ITEM LHCB_TOP_PACKAGES xqilla)
  LIST(REMOVE_ITEM LHCB_TOP_PACKAGES catboost)
  LIST(REMOVE_ITEM LHCB_TOP_PACKAGES tensorflow)
endif()

MESSAGE(STATUS "++++++++++ top packages: ${LHCB_TOP_PACKAGES}")
LCG_top_packages(${LHCB_TOP_PACKAGES})
