set(LCG_PYTHON_VERSION 3)
#---External packages----------------------------------------------
include(heptools-dev-base)
#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

#---Overwrites-----------------------------------------------------
LCG_external_package(ROOT           v6.22.00 )

#---Apple special issues---------------------------------------------
if(APPLE)
  # openloops fails building on MacOS (compiler crash)
  LCG_remove_package(openloops)
  LCG_remove_package(sherpa)
  LCG_remove_package(sherpa-openmpi)
  LCG_remove_package(herwig3)
  # Clash between R and ROOT headers (case insentivive) 
  LCG_remove_package(R)
  LCG_remove_package(rpy2)
  # Link error
  LCG_remove_package(unigen)
endif()
