#---List of externals--------------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Fix the versions for the most relevant packages -------------------
LCG_external_package(ROOT              v6.22.06                       )
LCG_external_package(VecGeom           v1.1.8                         )
LCG_external_package(clhep             2.4.4.0                        )
LCG_external_package(pybind11          2.5.0                          )

#---Define the top level packages for this stack-----------------------
LCG_top_packages(ROOT VecGeom Python Qt5 tbb XercesC clhep expat ninja 
                 ccache CMake lcgenv hdf5 valgrind pybind11)
