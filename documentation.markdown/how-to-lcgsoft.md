# Adding a new package to the DB

This should only be done once when building a package for the first time.

1. Update [packageinfo.txt](https://gitlab.cern.ch/sft/lcgcmake/blob/master/documentation/packageinfo.txt) with the description of new package
2. Login as `sftnight`
3. Setup the environment:

    ```
    source /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/packages/setup.sh 
    ```

4. Backup exising database:
   ```
    cd  /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/public_html/
    today=`date +%Y%m%d`

    cp db.backup db.backup.back$today
    cp db.sqlite3 db.sqlite.back$today
   ```

5. Run `lcgsoft/fill_description.py` (from `lcgjenkins` repository), passing directory containing the updated `packageinfo.txt` file

# Update release information in the DB
1. Login as `sftnight`
2. Setup the environment:

    ```
    source /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/packages/setup.sh 
    ```

3. Backup exising database:
   ```
    cd  /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/public_html/
    today=`date +%Y%m%d`

    cp db.backup db.backup.back$today
    cp db.sqlite3 db.sqlite.back$today
   ```

4. Prepare JSON file with `lcgsoft/ReleaseSummaryReader` script:
    ```
    lcgsoft/ReleaseSummaryReader <LCG_version> <platform> release
   ```

5. Push the prepared JSON file into the database:
    ```
    lcgsoft/fill_release.py -f <JSON file>  -d " " -e <date in YYYY-MM-DD format> 
    ```
