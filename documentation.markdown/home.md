The LCGSoft package allows distribution/installations of external generator packages
identical to the official LCGCMT releases for supported platforms. 

Please note that each LCGCMT release has its own set of supported platforms. The
list of LCG CMT Releases is avalaible [here](http://lcgsoft.web.cern.ch/lcgsoft/).

LCGsoft supports LCGCMT releases starting from 65.

Briefly speaking, LCGSoft perfoms the following actions:

1. Take source external/generators tarballs from the AFS directories:

    `/afs/cern.ch/sw/lcg/external/tarFiles/` - for externals;
    
    `/afs/cern.ch/sw/lcg/external/tarFiles/MCGeneratorsTarFiles` - for generators;

    with the corresponding http access:

    <http://service-spi.web.cern.ch/service-spi/external/tarFiles/>
    
    <http://service-spi.web.cern.ch/service-spi/external/tarFiles/MCGeneratorsTarFiles>

2. Untar source files and apply patches if needed. The patches applied can be found here: [externals](externals/patches), [projects](projects/patches), [pyexternals](pyexternals/patches), [gridexternals](gridexternals/patches), [generators](generators/patches).

    Each patch has a package version as a part of patch file name, it is applied only to this version.

3. Then LCGSoft package executes a set of actions using the macro based on cmake LCGPackage_Add. Default actions, performed automatically:
    * add the log files to the install area 
    * add the sources to the install area
    * create a binary tarball
    * strip rpath from .so files    

    A typical set of directives for the generator package configuration and installation steps (first 
impression that it is complicated but it is not) looks like this:

    ```
#---lhapdf6------------------------------------------------------------------------------------------
    LCGPackage_Add(
      lhapdf6
      URL ${gen_url}/LHAPDF-<NATIVE_VERSION>.tar.gz
      CONFIGURE_COMMAND ./configure
         --prefix=<INSTALL_DIR>
         --with-boost=${Boost_home}
         --with-yaml-cpp=${yamlcpp_home}
         PYTHON=${Python_home}/bin/python
         CYTHON=${cython_home}/bin/cython
         PYTHONPATH=${cython_pythonpath}  CFLAGS=-O2 CXXFLAGS=-O2 FCFLAGS=-O2 LDFLAGS=-O2
      BUILD_COMMAND ${MAKE} all
      ${library_path}=${yamlcpp_home}/lib:$ENV{${library_path}} PYTHONPATH=${cython_pythonpath} LIBRARY_PATH=${Python_home}/lib
      BUILD_IN_SOURCE 1
      INSTALL_COMMAND make install PYTHONPATH=${cython_pythonpath} LIBRARY_PATH=${Python_home}/lib
              COMMAND ${CMAKE_COMMAND} -DINSTALL_DIR=<INSTALL_DIR> -P ${CMAKE_SOURCE_DIR}/cmake/scripts/StripLib.cmake
      DEPENDS Boost yamlcpp Python cython
    )
    ```

    The `URL` and `CONFIGURE_COMMAND` directives are obligatory, others are optional.
    The directive names are more or less self-explainatory. User can easily change them according to his/her own needs.


    The LCGSoft package has a system of different level tests. The corresponding instructions are [here](https://gitlab.cern.ch/sft/lcgcmake/wikis/CTests)
