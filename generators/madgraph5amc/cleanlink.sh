#!/bin/bash
# argument for this script is source dir for madgraph: <SOURCE_DIR>
echo "Removing dead symbolik links"
# the escaped semicolon does not work inside the cmake itself, so we move this to a separate script
find $1 -type l -exec test ! -e {} \; -ls -delete
