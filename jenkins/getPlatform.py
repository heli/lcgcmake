#!/bin/sh
# getPlatform.py  - Obtain the platform tag from current system

""":"
# To ensure MacOS to get new version format (e.g. 11.x and not 10.16)
export SYSTEM_VERSION_COMPAT=0
exec python3 "$0";
"""

import os, platform, string, re, subprocess
import distro # replacement of platform.linux_distribution https://github.com/nir0s/distro

arch = platform.machine()

system = platform.system()
flagarch = 0
flagcxx=0
#---Determine external architecture and extra options----------------
if os.getenv('ARCHITECTURE'):
   architecture = os.getenv('ARCHITECTURE')
   flagarch = 1
if os.getenv('CXXOPTIONS'):
   cxxoptions = os.getenv('cxxoptions')
   flagcxx = 1
#---Determine the OS and version--------------------------------------
if system == 'Darwin' :
   if int(platform.mac_ver()[0].split('.')[0]) > 10 :
     osvers = 'mac' + platform.mac_ver()[0].split('.')[0]
   else:   
     osvers = 'mac' + "".join(platform.mac_ver()[0].split('.')[:2])
elif system == 'Linux' :
   dist = distro.linux_distribution()
   if re.search('SLC', dist[0]):
      osvers = 'slc' + dist[1].split('.')[0]
   elif re.search('CentOS', dist[0]):
      osvers = 'centos' + dist[1].split('.')[0]
   elif re.search('Alma', dist[0]):
      osvers = 'el' + dist[1].split('.')[0]
   elif re.search('Ubuntu', dist[0]):
      osvers = 'ubuntu' + dist[1].split('.')[0] + dist[1].split('.')[1]
   elif re.search('Fedora', dist[0]):
      osvers = 'fc' + dist[1].split('.')[0]
   elif re.search('Red Hat', dist[0]):
      osvers = 'redhat' + dist[1].split('.')[0]
   else:
      name, version, codename = distro.linux_distribution(full_distribution_name=False)
      osvers =  name + "".join(version.split('.')[:2])
elif system == 'Windows':
   osvers = 'win' + platform.win32_ver()[0]
else:
   osvers = 'unk-os'

#---Determine the compiler and version--------------------------------
if os.getenv('COMPILER') and os.getenv('COMPILER') != 'native' and not os.getenv("CC") :
  compiler = os.getenv('COMPILER')
else:
  if os.getenv('CC'):
    ccommand = os.getenv('CC')
  elif system == 'Windows':
    ccommand = 'cl'
  elif system == 'Darwin':
    ccommand = 'clang'
  else:
    ccommand = 'gcc'
  suffix = ''
  if ccommand == 'cl':
    versioninfo = subprocess.check_output([ccommand]).decode('utf-8')
    patt = re.compile('.*Version ([0-9]+)[.].*')
    mobj = patt.match(versioninfo)
    compiler = 'vc' + str(int(mobj.group(1))-6)
  elif ccommand.endswith('gcc') or 'gcc' in ccommand.split('/')[-1]:
     versioninfo = subprocess.check_output([ccommand,'-dumpversion']).decode('utf-8')
     patt = re.compile('([0-9]+)(\\.([0-9]+))?')
     mobj = patt.match(versioninfo)
     if int(mobj.group(1)) >= 7 :
        compiler = 'gcc' + mobj.group(1)
     else :  
        compiler = 'gcc' + mobj.group(1) + mobj.group(3)
#-------  if gcc versions contains .fp (built with frame-pointer enabled) then add fp suffix to platform  
     if ('gcc/'+ versioninfo.strip() + '.fp') in ccommand:
        compiler+='fp'
  elif ccommand.endswith('clang'):
     versioninfo = subprocess.check_output([ccommand,'-v'],stderr=subprocess.STDOUT).decode('utf-8')
     patt = re.compile('.*version ([0-9]+)[.]([0-9]+)')
     mobj = patt.match(versioninfo)
     if int(mobj.group(1)) >= 8 and system != 'Darwin':
        compiler = 'clang' + mobj.group(1)
     else:
        compiler = 'clang' + mobj.group(1) + mobj.group(2)
  elif ccommand.endswith('icc'):
     versioninfo = subprocess.check_output([ccommand, '-dumpversion']).decode('utf-8')
#     patt = re.compile('([0-9]+)')
     patt = re.compile('([0-9]+)\\.([0-9]+)')
     mobj = patt.match(versioninfo)
#     compiler = 'icc' + mobj.group(1) + mobj.group(2)
     compiler = 'icc' + mobj.group(1)
  else:
     compiler = 'unk-cmp'

#---Determine build type-------------------------------------------
if os.getenv('BUILDTYPE'):
   buildtype = os.getenv('BUILDTYPE')
else:
   buildtype = 'Release'

if buildtype == 'Release' : bt = 'opt'
elif buildtype == 'Debug' : bt = 'dbg'
else : bt = 'unk'

if flagarch==1:
   print('%s+%s-%s-%s-%s' %  (arch, architecture, osvers, compiler, bt))
elif flagcxx==1:
   print('%s+%s-%s-%s-%s' %  (arch, cxxoptions, osvers, compiler, bt))
elif flagarch==1 and flagcxx==1:
   print('%s+%s+%s-%s-%s-%s' %  (arch, architecture, cxxoptions, osvers, compiler, bt))
else:
   print('%s-%s-%s-%s' %  (arch, osvers, compiler, bt))
