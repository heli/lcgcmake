//----------------------------------------------------------------------------------------------------------------------
// This declarative Jenkins pipeline encodes all the steps required for the nightly of a single platform.
// Other jobs may call this pipelinbe to execute the build, test and installation of a set platforms.
//
// Author: Pere Mato
//----------------------------------------------------------------------------------------------------------------------

//---Global Functions and Maps------(specific to this pileline)---------------------------------------------------------
def lcg
def getCDashTrack() { return params.LCG_VERSION =~ 'dev' ? params.LCG_VERSION : 'Experimental'}
def getTarget() { return params.TARGET != 'all' ? params.TARGET : (params.LCG_VERSION =~ 'BE|AdePT|lhcb') ? 'top_packages' : 'all' }

pipeline {
  //---Global Parameters------------------------------------------------------------------------------------------------
  parameters {
    string(name:       'LCG_VERSION',          defaultValue: 'dev4', description: 'LCG stack version to build')
    string(name:       'COMPILER',             defaultValue: 'gcc10', description: 'Compiler tag (e.g. native, gcc8, gcc10, clang10, clang11)')
    choice(name:       'BUILDTYPE',            choices: ['Release', 'Debug'])
    string(name:       'LABEL',                defaultValue: 'centos7', description: 'Jenkins label for the OS/Platform or Docker image')
    string(name:       'HOST_ARCH',            defaultValue: 'x86_64', description: 'Jenkins label for the main architecture')
    string(name:       'DOCKER_LABEL',         defaultValue: 'docker-host-noafs', description: 'Label for the the nodes able to launch docker images')
    string(name:       'ARCHITECTURE',         defaultValue: '', description: 'Complement of the architecture (instruction set)')

    string(name:       'LCG_INSTALL_PREFIX',   defaultValue: '/cvmfs/sft.cern.ch/lcg/latest:/cvmfs/sft.cern.ch/lcg/releases', description: 'Location to look for already installed packages matching version and hash value. Leave blank for full rebuilds')
    string(name:       'LCG_ADDITIONAL_REPOS', defaultValue: 'https://lcgpackages.web.cern.ch/tarFiles/latest', description: 'Additional binary repository')
    string(name:       'LCG_EXTRA_OPTIONS',    defaultValue: '-DLCG_SOURCE_INSTALL=OFF;-DLCG_TARBALL_INSTALL=OFF', description: 'Additional configuration options to be added to the cmake command')
    string(name:       'LCG_IGNORE',           defaultValue: '', description: 'Packages already installed in LCG_INSTALL_PREFIX to be ignored (i.e. full rebuild is required). The list is \'single space\' separated.')
    booleanParam(name: 'USE_BINARIES',         defaultValue: true, description: 'Use binaries in repositories')
    string(name:       'TARGET',               defaultValue: 'all', description: 'Target to build (all, <package-name>, ...)')
    string(name:       'BUILDMODE',            defaultValue: 'Experimental', description: 'CDash mode')
  }
  //---Options----------------------------------------------------------------------------------------------------------
  options {
    buildDiscarder(logRotator( daysToKeepStr: '14' ))
    timestamps()
  }
  //---Additional Environment Variables---------------------------------------------------------------------------------
  environment {
    CDASH_TRACK = 'Experimental'
    CTEST_MODEL = 'Experimental'
    TARGET = getTarget()
    GITHUB_PAT = credentials('lcgcmake_github_pat')
    PYTHONUNBUFFERED = 'True'
  }

  agent none

  stages {
    //------------------------------------------------------------------------------------------------------------------
    //---Environment Stage to load and set global parameters------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('Environment') {
      agent { label 'startup-jobs' }
      steps {
        script {
          //---Change the build name------------------------------------------------------------------------------------
          def BTYPE = [Release: 'opt', Debug: 'dbg']
          currentBuild.displayName = "#${BUILD_NUMBER}" + ' ' + params.LCG_VERSION + '-' + params.HOST_ARCH + '-' + params.LABEL + '-' + params.COMPILER + '-' + BTYPE[params.BUILDTYPE]
          //---Load common variables and functions between several pilelines--------------------------------------------
          lcg = load 'lcgcmake/jenkins/Jenkinsfunctions.groovy'
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------
    //---Build stages---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------
    stage('InDocker') {
      when {
        beforeAgent true
        expression { params.LABEL =~ 'centos|ubuntu|slc|alma' }
      }
      agent {
        docker {
          alwaysPull true
          image params.HOST_ARCH =~ "aarch64" ? "gitlab-registry.cern.ch/sft/docker/${LABEL}:aarch64" : "gitlab-registry.cern.ch/sft/docker/$LABEL"
          label params.HOST_ARCH =~ "aarch64" ? "arm64-docker"  : "$DOCKER_LABEL"
          args  """-v /cvmfs:/cvmfs 
                   -v /ccache:/ccache 
                   -v /ec:/ec
                   -v /var/run/nscd:/var/run/nscd
                   -v /eos/project-n:/eos/project-n
                   -e SHELL
                   -e USER
                   -e container=docker
                   -e KRB5CCNAME="FILE:/tmp/krb5cc_14806"
                   --net=host
                   --hostname ${LABEL}-docker
                """
        }
      }
      stages {
        stage('Build') {
          steps {
            script {
              lcg.buildPackages()
            }
          }
        }
      }
    }
    stage('InBareMetal') {
      when {
        beforeAgent true
        expression { params.LABEL =~ 'mac' }
      }
      agent {
        label "$LABEL"
      }
      stages {
        stage('Build') {
          steps {
            script {
              lcg.buildPackages()
            }
          }
        }
      }
    }
  }
}
