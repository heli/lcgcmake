cmake_minimum_required(VERSION 3.0)

#---Common LCGCMake script----------------------------------------------------------------------------------------------
include(${CTEST_SCRIPT_DIRECTORY}/lcgcmake-common.cmake)

find_program(CTEST_GIT_COMMAND NAMES git)
set(CTEST_UPDATE_COMMAND ${CTEST_GIT_COMMAND})
if(NOT "$ENV{GIT_COMMIT}" STREQUAL "")  #--- From Jenkins---------------------
  # set(CTEST_CHECKOUT_COMMAND "cmake -E chdir ${CTEST_SOURCE_DIRECTORY} ${CTEST_GIT_COMMAND} checkout -f $ENV{GIT_PREVIOUS_COMMIT}")
  set(CTEST_GIT_UPDATE_CUSTOM  ${CTEST_GIT_COMMAND} checkout -f $ENV{GIT_COMMIT})
endif()


#---From Jenkins--------------------------------------------------------------------------------------------------------
if(NOT "$ENV{GIT_COMMIT}" STREQUAL "")
  set(CTEST_GIT_UPDATE_CUSTOM  ${CTEST_GIT_COMMAND} checkout -f $ENV{GIT_COMMIT})
endif()

set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_COMMAND "make -k -j${ncpu} $ENV{TARGET}")
set(CTEST_BUILD_NAME $ENV{LCG_VERSION}-${CTEST_BUILD_NAME})

#---Notes to be attached to the build-----------------------------------------------------------------------------------
set(CTEST_NOTES_FILES ${CTEST_NOTES_FILES} ${CTEST_SOURCE_DIRECTORY}/cmake/toolchain/heptools-$ENV{LCG_VERSION}.cmake)
set(CTEST_NOTES_FILES ${CTEST_NOTES_FILES} ${CTEST_BINARY_DIRECTORY}/dependencies.json)

#---CTest commands------------------------------------------------------------------------------------------------------
file(REMOVE_RECURSE ${CTEST_BINARY_DIRECTORY})

if("$ENV{CLEAN_INSTALLDIR}" STREQUAL "true")
  file(REMOVE_RECURSE ${CTEST_INSTALL_DIRECTORY})
elseif("$ENV{USE_BINARIES}" STREQUAL "true")
  file(REMOVE_RECURSE ${CTEST_INSTALL_DIRECTORY}/$ENV{LCG_VERSION})
endif()

#---Options-------------------------------------------------------------------------------------------------------------
set(ignore $ENV{LCG_IGNORE})
set(ignore "${ignore}")
set(options -DLCG_VERSION=$ENV{LCG_VERSION}
            -DCMAKE_INSTALL_PREFIX=${CTEST_INSTALL_DIRECTORY}
            -DPDFsets=$ENV{PDFSETS}
            -DLCG_INSTALL_PREFIX=$ENV{LCG_INSTALL_PREFIX}
            -DLCG_SAFE_INSTALL=ON
            -DLCG_IGNORE=${ignore}
            -DCMAKE_VERBOSE_MAKEFILE=OFF
            $ENV{LCG_EXTRA_OPTIONS})


if("$ENV{USE_BINARIES}" STREQUAL "true")
   list(APPEND options -DUSE_BINARIES=ON)
   list(APPEND options -DLCG_ADDITIONAL_REPOS=$ENV{LCG_ADDITIONAL_REPOS})
endif()

if(NOT "$ENV{ARCHITECTURE}" STREQUAL "")
   list(APPEND options -DLCG_INSTRUCTIONSET=$ENV{ARCHITECTURE})
endif()

if(APPLE)
   list(APPEND options -DCMAKE_C_COMPILER=/usr/bin/clang -DCMAKE_CXX_COMPILER=/usr/bin/clang++)
endif()

file(REMOVE ${CTEST_BINARY_DIRECTORY}/isDone-$ENV{PLATFORM}
            ${CTEST_BINARY_DIRECTORY}/isDone-unstable-$ENV{PLATFORM})

# ---The build mode drives the name of the slot in cdash----------------------------------------------------------------
ctest_start($ENV{CTEST_MODEL} TRACK $ENV{CDASH_TRACK})
ctest_update()

ctest_configure(BUILD   ${CTEST_BINARY_DIRECTORY} 
                SOURCE  ${CTEST_SOURCE_DIRECTORY}
                OPTIONS "${options}")

if(NOT DEFINED ENV{DO_BUILD} OR "$ENV{DO_BUILD}" STREQUAL "true")

  ctest_build(BUILD ${CTEST_BINARY_DIRECTORY} RETURN_VALUE rc)

  if(${rc} EQUAL 0)
    file(TOUCH ${CTEST_BINARY_DIRECTORY}/isDone-$ENV{PLATFORM})
  else()
   file(TOUCH ${CTEST_BINARY_DIRECTORY}/isDone-unstable-$ENV{PLATFORM})
  endif()

  if(NOT "$ENV{TARGET}" STREQUAL all)
    file(GLOB stamps  RELATIVE ${CTEST_BINARY_DIRECTORY}/timestamps ${CTEST_BINARY_DIRECTORY}/timestamps/*-stop.timestamp)
    string(REPLACE "-stop.timestamp" "" stamps "${stamps}")
    list(SORT stamps CASE INSENSITIVE)
    string(JOIN " " packages ${stamps})
    execute_process(COMMAND date +%Y-%m-%d--%H:%M:%S OUTPUT_VARIABLE date OUTPUT_STRIP_TRAILING_WHITESPACE)
    set(textfile ${CTEST_BINARY_DIRECTORY}/LCG_$ENV{LCG_VERSION}_$ENV{PLATFORM}.txt)
    file(READ ${textfile} contents)
    file(WRITE ${textfile} "# ${date} PKGS_OK ${packages}\n")
    file(APPEND ${textfile} ${contents})
  endif()

  if("$ENV{RUN_TESTS}" STREQUAL "true" AND "$ENV{TARGET}" MATCHES "all|top_packages")
   ctest_test(PARALLEL_LEVEL ${ncpu} INCLUDE_LABEL "${CTEST_LABELS}")
  endif()

  file(STRINGS ${CTEST_BINARY_DIRECTORY}/fail-logs.txt logs)
  ctest_upload(FILES ${logs})

  ctest_submit(PARTS Update Configure Build Test Notes Upload)
else()
  ctest_submit(PARTS Update Configure Notes)
endif()