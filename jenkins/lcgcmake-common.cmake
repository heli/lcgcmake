#---Utility Macros----------------------------------------------------------
include(${CTEST_SCRIPT_DIRECTORY}/lcgcmake-macros.cmake)

#---Make sure that VERBOSE is OFF to avoid screwing up the build performance
unset(ENV{VERBOSE})

#---General Configuration---------------------------------------------------
GET_PWD(pwd)
GET_HOST(host)
GET_NCPUS(ncpu)
GET_CONFIGURATION_TAG(tag)
GET_CTEST_BUILD_NAME(CTEST_BUILD_NAME)

set(CTEST_BUILD_CONFIGURATION $ENV{BUILDTYPE})
set(CTEST_CONFIGURATION_TYPE ${CTEST_BUILD_CONFIGURATION})

#---Set the source and build directory--------------------------------------
set(CTEST_BUILD_PREFIX "$ENV{WORKSPACE}")
set(CTEST_SOURCE_DIRECTORY "${CTEST_BUILD_PREFIX}/lcgcmake")
set(CTEST_BINARY_DIRECTORY "${CTEST_BUILD_PREFIX}/build")
set(CTEST_INSTALL_DIRECTORY "${CTEST_BUILD_PREFIX}/install")

if(DEFINED ENV{BUILDHOSTNAME})
  set(CTEST_SITE $ENV{BUILDHOSTNAME})
elseif(DEFINED ENV{container} AND DEFINED ENV{NODE_NAME})
  set(CTEST_SITE "$ENV{NODE_NAME}-$ENV{container}")
else()
  set(CTEST_SITE "${host}")
endif()

#---CDash settings----------------------------------------------------------
set(CTEST_PROJECT_NAME "LCGSoft")
set(CTEST_NIGHTLY_START_TIME "01:00:00 UTC")
set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "lcgapp-cdash.cern.ch")
set(CTEST_DROP_LOCATION "/submit.php?project=LCGSoft")
set(CTEST_DROP_SITE_CDASH TRUE)

#---Custom CTest settings---------------------------------------------------
set(CTEST_CUSTOM_MAXIMUM_FAILED_TEST_OUTPUT_SIZE "1000000")
set(CTEST_CUSTOM_MAXIMUM_PASSED_TEST_OUTPUT_SIZE "100000")
set(CTEST_CUSTOM_MAXIMUM_NUMBER_OF_ERRORS "256")
set(CTEST_TEST_TIMEOUT 1500)
set(CTEST_CUSTOM_ERROR_EXCEPTION ${CTEST_CUSTOM_ERROR_EXCEPTION}
        ": No such file or directory"
        "llvm-objdump: error:")


