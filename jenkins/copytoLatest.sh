#!/bin/bash -x
# This script is intended to copy binary tarfiles to the 'latest' in EOS

THIS=$(dirname ${BASH_SOURCE[0]})
export EOS_MGM_URL=root://eosuser.cern.ch

$THIS/kinit.sh

basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/latest
node=lcgapp-centos7-physical.cern.ch
txtfile=LCG_${LCG_VERSION}_${PLATFORM}.txt

if [ -d $WORKSPACE/docker/build ]; then
  cd $WORKSPACE/docker/build
else
  cd $WORKSPACE/build
fi

if [[ ${PLATFORM} == *-slc6* || ${PLATFORM} == *-cc7* || ${PLATFORM} == *-centos* ||  ${PLATFORM} == *-el9* || ${PLATFORM} == *-ubuntu* || ${PLATFORM} == *-mac* ]]; then
  xrdcp -f $txtfile $EOS_MGM_URL/$basespace/$txtfile
  if [ "$(ls -A tarfiles)" ]; then
    echo "Take action: tarfiles is not Empty"
    cd tarfiles
    for file in *.tgz; do
      xrdcp -f ${file} $EOS_MGM_URL/$basespace/${file}
    done
  else
    echo "tarfiles is Empty. I will assume this is not an error though"
    exit 0
  fi
  echo Generating summary file for $PLATFORM
  IFS='-' read -ra TOK <<< "$PLATFORM"
  arch=$(echo ${TOK[0]} | cut -d'+' -f 1); osver=${TOK[1]}; comp=${TOK[2]}; type=${TOK[3]}
  barePLATFORM=${arch}-${osver}-${comp}-${type}
  xrdfs ${EOS_MGM_URL} ls $basespace | grep -e ${arch}.*-${osver}-${comp}-${type}\.tgz | grep -v '.sys.v#.' | sed s:${basespace}/:: >  summary-$barePLATFORM.txt
  xrdcp -f summary-$barePLATFORM.txt $EOS_MGM_URL/$basespace/summary-$barePLATFORM.txt
else
  scp -B $PWD/$txtfile sftnight@$node:$basespace
  if [ "$(ls -A tarfiles)" ]; then
    cd tarfiles
    files=`ls -1 -d *.tgz`
    for file in $files;  do
        scp -B $PWD/$file sftnight@$node:$basespace
    done
  else
    echo "tarfiles directory is empty. I will assume this is not an error though"
    exit 0
  fi
  ssh sftnight@$node "$basespace/make-summaries $PLATFORM"
fi
exit 0
