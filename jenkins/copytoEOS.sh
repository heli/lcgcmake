#!/bin/bash -x

# set -e  # Fail on first error
THIS=$(dirname ${BASH_SOURCE[0]})

export EOS_MGM_URL="root://eosproject-l.cern.ch"
# export PLATFORM=$(grep $WORKSPACE/properties.txt -e PLATFORM | cut -d'=' -f2)
FILES=*.tgz
weekday=`date +%a`
txtfile=LCG_${LCG_VERSION}_${PLATFORM}.txt
isDone=isDone-${PLATFORM}
isDoneUnstable=isDone-unstable-${PLATFORM}

# Retries a command a with an exponential backoff strategy.
# Based on https://gist.github.com/fernandoacorreia/b4fa9ae88c67fa6759d271b743e96063
function retry {
    local current_attempt=1
    local max_attempts=5
    local timeout_seconds=1
    set -o pipefail
    while (( ${current_attempt} <= ${max_attempts} ))
    do
        "$@"
        if [[ $? == 0 ]]; then
            # Command successful
            break
        elif (( ${current_attempt} >= ${max_attempts} )); then
            echo
            echo "[ERROR] Attempt ${current_attempt}/${max_attempts} failed ($@). Maximum retries exceeded. Attempting to continue." 1>&2
        else
            echo
            echo "[WARNING] Attempt ${current_attempt}/${max_attempts} failed ($@). Retrying in ${timeout_seconds}s..." 1>&2
            sleep ${timeout_seconds}
        fi
        current_attempt=$(( current_attempt + 1 ))
        timeout_seconds=$(( timeout_seconds * 2 ))
    done
}

# Make sure we have credentials
$THIS/kinit.sh

# Check xrootd coommands
_xfs=`which xrdfs` >/dev/null 2>&1
if [[ $? != 0 ]]; then
    echo "xrdfs not available! Aborting."; exit 1
else
    XFS="${_xfs} ${EOS_MGM_URL}"
fi
XCP=`which xrdcp` >/dev/null 2>&1
if [[ $? != 0 ]]; then
    echo "xrdcp not available! Aborting."; exit 1
fi
XCS=`which xrdadler32` >/dev/null 2>&1
if [[ $? != 0 ]]; then
    echo "xrdadler32 not available! Aborting."; exit 1
fi

# Relevant dirs
lcgTarDir=/eos/project/l/lcg/www/lcgpackages/tarFiles
if [ "${BUILDMODE}" == "nightly" ]; then
    basespace=${lcgTarDir}/nightlies/${LCG_VERSION}/${weekday}
else
    basespace=${lcgTarDir}/releases
    metaspace=/eos/project/l/lcg/www/lcgpackages/lcg/meta
fi

# Cope with non-docker builds
if [ -d $WORKSPACE/docker/build ]; then
   cd $WORKSPACE/docker/build
else
   cd $WORKSPACE/build
fi

if [[ ${PLATFORM} == *-slc6* || ${PLATFORM} == *-cc7* || ${PLATFORM} == *-centos7* || \
      ${PLATFORM} == *-ubuntu* || ${PLATFORM} == *-fedora* || ${PLATFORM} == *-fc* || \
      ${PLATFORM} == *-cc8* || ${PLATFORM} == *-cs8* || ${PLATFORM} == *-centos8* || \
      ${PLATFORM} == *-cc9* || ${PLATFORM} == *-cs9* || ${PLATFORM} == *-centos9* || \
      ${PLATFORM} == *-el9* || \
      ${PLATFORM} == *-mac* ]]; then

    # Check that EOS is mounted and available
    ${XFS} stat ${lcgTarDir} 1>/dev/null || (echo "EOS not available! Aborting."; exit 1)

    if [ "${BUILDMODE}" == "nightly" ]; then
        # Make sure the directory exists
        ${XFS} mkdir -p ${basespace}
        if [[ $? != 0 ]]
        then
            echo "Could not create ${basespace}! err = $?. Aborting."
            exit 1
        fi
        # Clear 'isDone'
        ${XFS} stat ${basespace}/${isDone} > /dev/null 2>&1
        if [[ $? == 0 ]]; then
            ${XFS} rm ${basespace}/${isDone}
            if [[ $? != 0 ]]; then
                echo "Could not remove ${basespace}/${isDone}! err = $?. Aborting."
                exit 1
            fi
        fi
        # Clear 'isDoneUnstable'
        ${XFS} stat ${basespace}/${isDoneUnstable} > /dev/null 2>&1
        if [[ $? == 0 ]]; then
            ${XFS} rm ${basespace}/${isDoneUnstable}
            if [[ $? != 0 ]]; then
                echo "Could not remove ${basespace}/${isDoneUnstable}! err = $?. Aborting."
                exit 1
            fi
        fi
        # Copy updated isDone/Unstable files
        if [ -f $isDone ]; then
            retry ${XCP} $isDone ${EOS_MGM_URL}/$basespace/$isDone
        fi
        if [ -f $isDoneUnstable ]; then
            retry ${XCP} $isDoneUnstable ${EOS_MGM_URL}/$basespace/$isDoneUnstable
        fi
    fi
    
    retry ${XCP} -f $txtfile ${EOS_MGM_URL}/$basespace/$txtfile
    if [ "${BUILDMODE}" != "nightly" ]; then
       retry ${XCP} -f $txtfile ${EOS_MGM_URL}/$metaspace/$txtfile
    fi

    if [ "$(ls -A tarfiles)" ]; then
        echo "Take action: tarfiles is not Empty"
        cd tarfiles
        for file in $FILES
        do
            if [ "${BUILDMODE}" == "nightly" -o "${BUILDMODE}" == "Rebuild" ]; then
                retry ${XCP} -f ${file} ${EOS_MGM_URL}/${basespace}/${file}
            else
                # Check if the file exists
                ${XFS} stat ${basespace}/${file} > /dev/null 2>&1
                if [[ $? == 0 ]]; then
                    echo "The file already exists in EOS"
                else
                    retry ${XCP} -f ${file} ${EOS_MGM_URL}/${basespace}/${file}
                fi
            fi
        done
        if [ "${BUILDMODE}" == "release" ]; then
            echo "Generating summary file for $PLATFORM"
            IFS='-' read -ra TOK <<< "$PLATFORM"
            arch=$(echo ${TOK[0]} | cut -d'+' -f 1); osver=${TOK[1]}; comp=${TOK[2]}; type=${TOK[3]}
            barePLATFORM=${arch}-${osver}-${comp}-${type}
            xrdfs ${EOS_MGM_URL} ls ${basespace} | grep -e ${arch}.*-${osver}-${comp}-${type}\.tgz | grep -v '.sys.v#.' | sed s:${basespace}/:: >  summary-$barePLATFORM.txt
            xrdcp -f summary-$barePLATFORM.txt $EOS_MGM_URL/${basespace}/summary-$barePLATFORM.txt
        fi
    else
        echo "tarfiles is Empty. I will assume this is not an error though"
        exit 0
    fi
else
    # Check that EOS is mounted and available
    #ls /eos/project/l/lcg/www/lcgpackages/tarFiles 1>/dev/null || (echo "EOS not available! Aborting."; exit 1)
    
    if [ "${BUILDMODE}" == "nightly" ]; then
        ssh sftnight@lcgapp-centos7-physical.cern.ch "rm -vf ${basespace}/${isDone}"
        ssh sftnight@lcgapp-centos7-physical.cern.ch "rm -vf ${basespace}/${isDoneUnstable}"
        retry scp ${isDone} ${isDoneUnstable} sftnight@lcgapp-centos7-physical.cern.ch:${basespace}
    fi

    ssh -o StrictHostKeyChecking=no sftnight@lcgapp-centos7-physical.cern.ch "rm -vf ${basespace}/${txtfile}"
    retry scp -o StrictHostKeyChecking=no ${txtfile} sftnight@lcgapp-centos7-physical.cern.ch:${basespace}

    if [ "$(ls -A tarfiles)" ]; then
        echo "Take action: tarfiles is not Empty"
        cd tarfiles
        retry scp -o StrictHostKeyChecking=no *tgz sftnight@lcgapp-centos7-physical.cern.ch:${basespace}
    else
        echo "tarfiles is Empty. I will assume this is not an error though"
        exit 0
    fi
fi
