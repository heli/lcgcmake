#!/bin/bash -x
# Wait until today's nightly is available on stratum 1
# wait_for_cvmfs $VOLUME $REVISION

VOLUME=$1
REVISION=$2

for iterations in {1..12}
do
    if [ `uname -s` == Darwin ]; then
        current_rev=`xattr -p user.revision /cvmfs/$VOLUME`
        if [ $? -ne 0 ]; then
            # Probably the CVMFS volume is not mounted. Try to mount it...
            # we need to mount cvmfs-config before we can mount the other repositories
            # needed for CVMFS_HTTP_PROXY config variable
            echo "Mounting CVMFS volume config-cvmfs.cern.ch"
            sudo mount -t cvmfs cvmfs-config.cern.ch /Users/Shared/cvmfs/cvmfs-config.cern.ch || true
            echo "Mounting CVMFS volume $VOLUME"
            sudo mount -t cvmfs $VOLUME  /Users/Shared/cvmfs/$VOLUME
            if [ $VOLUME != sft.cern.ch ]; then    # sft.cern.ch is also needed later for running the tests
              sudo mount -t cvmfs sft.cern.ch /Users/Shared/cvmfs/sft.cern.ch || true
            fi
            current_rev=`xattr -p user.revision /cvmfs/$VOLUME`
        fi
    else
        current_rev=`attr -qg revision /cvmfs/$VOLUME/`
    fi
    if [ "${current_rev}" -ge "${REVISION}" ]
    then
        echo "[INFO] Current revision [${current_rev}] is greater or equal than the required [${REVISION}]."
        break
    else
        if  [[ "${iterations}" == "36" ]]
        then
            echo "[ERROR] Abort after one hour of waiting."
            exit 1
        else
            echo "[WARNING] Revision is not yet present on CVMFS stratum 1. Going to sleep for 5 minutes ..."
            sleep 300
        fi
    fi
done
exit 0
