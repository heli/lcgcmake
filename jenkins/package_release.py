#!/usr/bin/python3
from __future__ import print_function

import os, sys, re
from subprocess import Popen
import common_parameters
from optparse import OptionParser
from copyRPMS import copyRPMS
from getNewRPMrevision import get_new_RPM_revision

def executeAndCheckCommand(cmd, success, error):
    if (executeCmd(cmd) == 0):
      print(success)
    else:
      print(error)
      sys.exit(1)

def executeCmd(cmd):
    print("EXECUTING: "+cmd)
    p=Popen(cmd,shell=True)
    p.wait()
    if p.stderr==None and p.returncode==0:
        return 0
    else: return 1

def extractLCGNumber(release):
    """ extract 79 from 79a or 79root6 """
    p = re.compile("^[0-9]*")
    return p.match(release).group()


def extractLCGSummary(scriptdir,platform,lcg_version, policy):
    command = "%s/extract_LCG_summary.py . %s %s %s" %(scriptdir, platform, lcg_version, policy)
    executeAndCheckCommand(command,"Extracting LCG summary","ERROR extracting LCG summary")

def createSpecFiles(workdir,platform,rpm_revision):
    command = "%s/LCGRPM/package/createLCGRPMSpec.py LCG_externals_%s.txt -b %s/packaging -o externals.spec --release %s" %(scriptdir, platform, workdir, rpm_revision)
    executeAndCheckCommand(command,"creating specfile for externals","ERROR creating specfile for externals")
    command = "%s/LCGRPM/package/createLCGRPMSpec.py LCG_generators_%s.txt -b %s/packaging -o generators.spec --release %s" %(scriptdir, platform, workdir, rpm_revision)
    executeAndCheckCommand(command,"creating specfile for generators","ERROR creating specfile for generators")

def buildRPMs():
    command = "rpmbuild -bb externals.spec"
    executeAndCheckCommand(command,"building externals RPMs", "ERROR building externals RPMs")
    command = "rpmbuild -bb generators.spec"
    executeAndCheckCommand(command,"building generators RPMs", "ERROR generators RPMs")

##########################
if __name__ == "__main__":
  # extract command line parameters
  usage = "usage: %prog scriptdir workdir lcg_version platform target policy"
  parser = OptionParser(usage)
  (options, args) = parser.parse_args()
  if len(args) != 6:
    parser.error("incorrect number of arguments.")
  else:
    scriptdir     =  os.path.dirname(os.path.abspath(__file__))
    workdir       =  args[0]
    lcg_version   =  args[1]
    platform      =  args[2]
    target        =  args[3]
    policy        =  args[4]
    compiler      =  args[5]

  if policy not in ("release", "Generators", "Rebuild"):
    print("ERROR: unknown install/copy policy")
    sys.exit(1)

  os.environ['EOS_MGM_URL'] = 'root://eosproject-l.cern.ch'

  command = os.path.join(os.path.dirname(os.path.realpath(__file__)),'kinit.sh')
  executeAndCheckCommand(command,"Token creation to write in EOS", "ERROR creating a Kerberos token")

  tmparea = common_parameters.tmparea

  # do the job
  extractLCGSummary(scriptdir,platform,lcg_version, policy)
  if policy in ("Generators", "Rebuild"):
    rpm_revision = get_new_RPM_revision(lcg_version, platform)
  else:
    rev_number = os.environ['RPM_REVISION_NUMBER'] if 'RPM_REVISION_NUMBER' in os.environ else 'a'
    if rev_number.isdigit():
      rpm_revision = rev_number
    else:
      rpm_revision = 1
  createSpecFiles(workdir,platform,rpm_revision)
  buildRPMs()
  rpmsarea = "%s/LCG_%s" %(tmparea, lcg_version)
  copyRPMS(workdir, rpmsarea, target=target, policy=policy)
