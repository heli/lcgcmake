#!/bin/bash -e
# special script for fontconfig package unreferening absolute symlinks inside etc/fonts/conf.d
fcinstdir="$1"

mydir=${fcinstdir}/etc/fonts/conf.d/

for f in $(find ${fcinstdir} -path "*/fontconfig/*/etc/fonts/conf.d/*" -type l -name "*-*.conf");do realfile=$(readlink $f); rm $f; cp ${realfile} $f;done;


