#!/bin/bash
# post-install macro to extract headers from special.zip
# usage:  postinstall <INSTALL_DIR>

if test "x$1" = "x" ; then
    echo "vectorclass_postinstall: install dir undefined!"
    exit 1
fi

uzipcmd=`which unzip`
if test ! "x$?" = "x0" ; then
    echo "vectorclass_postinstall: unzip not found"
    exit 1
fi

cd $1
hfile="special.zip"
if test -f $hfile ; then
    if test -r $hfile ; then
       $uzipcmd $hfile
    else
       echo "vectorclass_postinstall: $hfile not readable"
       stat $hfile
       exit 1
    fi
else
    echo "vectorclass_postinstall: $hfile not found"
    pwd
    exit 1
fi

exit 0
