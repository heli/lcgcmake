!/bin/bash

# Check that all required Qt6 libraries are present in specified directory
# Usage: ./check_qt6_libraries.sh /path/to/directory

DIRECTORY=$1

if [[ ! -d "$DIRECTORY" ]]; then
  echo "Error: specified directory does not exist."
  exit 1
fi

MISSING_LIBS=""

if [[ "$OSTYPE" == "darwin"* ]]; then
  LIBRARY_EXT=".dylib"
else
  LIBRARY_EXT=".so"
fi


for LIBRARY in libQt6Core libQt6Gui libQt6Widgets libQt6OpenGL libQt6PrintSupport libQt63DCore libQt63DExtras libQt63DRender; do
  if [[ ! -f "$DIRECTORY/$LIBRARY${LIBRARY_EXT}" ]]; then
    MISSING_LIBS="$MISSING_LIBS $LIBRARY${LIBRARY_EXT}"
  fi
done

if [[ -n "$MISSING_LIBS" ]]; then
  echo "Error: the following Qt6 libraries are missing from $DIRECTORY:$MISSING_LIBS"
  exit 1
else
  echo "All required Qt6 libraries are present in $DIRECTORY."
  exit 0
fi
